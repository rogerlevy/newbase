\ {nodoc} tell the documentation progress monitor tool to ignore this file
true constant config-sys-sf
true constant config-arch-x86
true constant config-os-windows

include index-sf

\ while documenting, this saves a step of having to load the progress monitor tool manually.
fload docprogress
