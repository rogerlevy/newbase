\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ notes

\ regions
\ you don't have to worry about broken links
\ more efficient than heap allocations
\ static memory that can be freed
\ words can be treated dynamically or statically
\ regions are allocated and compiled into program's memory



\ prototype
s@
s!
* ifield
* old-structs
* create-field
* general-struct

field -- base word for defining fields
var
dvar

>o  -- object stack
o>
save-s!
restore-s
with
&
[&]


prototype
initialize -- copies default values for properties into given address
dict
sysheap -- not used anywhere; but designed to create objects on the heap
instantiate -- should be 'new' but new is already defined 
instance -- always in the dictionary
instance, -- same as instance except no return address
clone
object -- same as instance, but allows you to name the instance

pdef -- current class definition
+innards --
-innards
in -- useful when debugging classes

fields
begetting
adressing:  -- usage: "addressing: ep @ + ;"


[i -- to implement private code until closing i]
i]

reopen
sizeof

%class -- 
%object

>prototype -- fetches prototype
new-structs
phere  - prototype here (get the address following current class definition)
var:
fvar:
sfvar:
init: - create a field of unknown type and unknown size
^ - comma a value into the class structure
string:
noop:
inline
~> -- evolution operator
endp
reopen
pstruct


\ quicksort

cellsort - sorts a contiguous array



\ array

\ update array stack diagrams to have <stack> instead of  <array>

%array
items
>items
* length -- rename to number of items
each
<each -- back each
cell[]
[]
first[]
last[]
push
pop
top@
vacate
\ need to rethink these many words for creating and initializing stacks and arrays
*stack -- 
init-stack
init-array
arrayheader,
items!
array!
stack!

stack, -- create a stack
array, -- create an array

\ (by default uses s register for basic structures)
/each -- begin iteration
each/ -- end iteration
+item -- go to next item 

check -- essentially a 'contains' operation on an array \ should be renamed to contains

those -- iteration for all items in an array
<those -- back those -- reverse iteration through array

sort  -- sorts cell sized values

rnditem -- get address and index of a random item

rnd@ -- get random item's value

rnd[] -- get random item address

[array -- (canot be used at compilation time) \ create arr cell [array 0 , 2 , 3 , array]
array]

[]wrap
[]clamp
wrap[]   -- desire these
clamp[]  -- ''

shuffle --


\ vtable

vt
%vtable
action
actions:
::    -- override operator
>::
don't
vbeget
vtable,
vtable
action-type

\ tweening
ease-in
ease-out
ease-in-out
no-ease
%tween
tweens
tween:step
tween
clear-tweens
process-tweens

linear
exponential
sine
quadratic
cubic
quartic
quintic
circular
overshoot



Roger: need to look into a new subclassing operator that supports different classclass-es

Roger: need to look at onbeget var stack diagram in prototype. it appears to be incorrect.






\ afterschool


\ stackvecs2d
\ fix vecfrom or grab old version that works

\ gameutils







