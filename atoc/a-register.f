\ ==============================================================================
\ ForestLib
\ A register
\ ========================= copyright 2014 Roger Levy ==========================

variable 'a \ the A register pointer

\ intent: fetch from the A register 
\ usage: a@
: a@  ( - adr )  \ fetch A register
  " 'a @ " evaluate ; immediate

\ intent: store to A register
\ usage: <addr> a!
: a!  ( adr - )  \ store A register
  " 'a ! " evaluate ; immediate

\ intent: fetch from address found in A register then increment A register pointer by 1 cell
\ usage: @+
: @+  ( - n )  \ fetch address; A register next
  " 'a @ @  cell 'a +! " evaluate ; immediate

\ intent: store to address found in A register then increment A register pointer by 1 cell
\ usage: <n> !+
: !+  ( n - )  \ store address; A register next
  " 'a @ !  cell 'a +! " evaluate ; immediate

\ intent: store two values to address found in A register then increment A register pointer by 2 cells
\ usage: <x> <y> 2!+
macro: 2!+  swap !+ !+ ;  ( x y - )  \ store vector in address A register next

\ intent: store to address found in A register
\ usage: <n> !a
macro: !a   'a @ ! ;  ( n - )  \ store address A register

\ intent: fetch from address found in A register
\ usage: @a
macro: @a   'a @ @ ;  ( - n )  \ fetch address A register

\ intent: increment A register pointer by <n> bytes
\ usage: <n> +a
macro: +a   'a +! ;  ( n - )  \ increment A register pointer

\ intent: store address in A for remainder of definition (auto-restored on return)
\ usage: <addr> a!>
: a!>  ( adr - )  \ a-store-for: 
  r>  'a @ >r  swap 'a !  call  r> 'a ! ;

\ intent: fetch character from address in A register then add 1 to address in A register
\ usage: c@+
macro: c@+  'a @ c@  'a ++ ;  ( - c )  \ fetch char from A address

\ intent: store character to address in A register then add 1 to address in A register
\ usage: <character> c!+
macro: c!+  'a @ c!  'a ++ ;  ( c - )  \ store char to A address

\ intent: store 16-bit value to address in A register then add 2 bytes to address in A register
\ usage: <h> h!+
: h!+  ( h - )  \ store 16-bit value to A address
  " 'a @ h!  2 'a +! " evaluate ; immediate

\ intent: fetch 16-bit value from address in A register then add 2 bytes to address in A register
\ usage: h@+
: h@+  ( - h )  \ fetch 16-bit value from A address
  " 'a @ h@  2 'a +! " evaluate ; immediate

\ intent: fetch 32-bit float from address in A register then increment A register pointer by 1 cell
\ usage: sf@+
macro: sf@+   'a @ sf@  cell 'a +! ;  ( - f: n )  \ fetch 32-bit float from A address

\ intent: store 32-bit float to address in A register then increment A register pointer by 1 cell
\ usage: <float> sf!+
macro: sf!+   'a @ sf!  cell 'a +! ;  ( f: n - )  \ store 32-bit float to A address
