\
\ note: imove takes number of cells
\

[defined] x86 [if]
  \ intent: copy <size> cells of memory from <source> address to <destination> address
  \ usage: <source> <destination> <size> imove
  CODE imove  ( from to count - )  \ copy memory
    EBX EBX TEST   0= NOT IF
      0 [EBP] EDX MOV   4 [EBP] ECX MOV
      BEGIN   0 [ECX] EAX MOV   EAX 0 [EDX] MOV
      4 # ECX ADD  4 # EDX ADD   EBX DEC   0= UNTIL
    THEN   8 [EBP] EBX MOV   12 # EBP ADD
  RET   END-CODE

  \ intent: fill <size> cells of memory at given address with <value>
  \ usage: <addr> <size> <value> ifill
  CODE ifill  ( c-addr count val - )  \ fill memory with value
    0 [EBP] ECX MOV   ECX ECX TEST        \ Get count, skip if 0
    0= NOT IF   4 [EBP] EDX MOV           \ Get string addr
      BEGIN   EBX 0 [EDX] MOV             \ Write char
      4 # EDX ADD   ECX DEC   0= UNTIL    \ Increment pointer, loop
    THEN   8 [EBP] EBX MOV                \ Drop parameters
    12 # EBP ADD  RET   end-code
[else]
  \ intent: copy <size> cells of memory from <source> address to <destination> address
  \ usage: <source> <destination> <size> imove
  : imove  ( from to count - )  \ copy memory
    cells move ;

  \ intent: fill <size> cells of memory at given address with <value>
  \ usage: <addr> <size> <value> ifill
  : ifill  ( c-addr count val - )  \ fill memory with value
    -rot  swap a!>  0 ?do  dup !+  loop  drop ;
[then]

\ intent: fill <size> cells of memory at given address with zero
\ usage: <addr> <size> ierase
: ierase   ( adr count - )  \ zero-out memory
  0 ifill ;
