
[defined] config-sys-vfx [if]

\ intent: concatenate a string
\ usage: {not to be used externally}
  : (prepend$) ( from len to - )  \ concatenate string
    2dup 2>r  count + swap move  2r> c+! ;

\ intent: substitute one word with another
\ usage: subst <old> <new>
  : subst  ( - <old> <new> )  \ substitute one word with another
    s" synonym " pocket place  bl parse pocket (prepend$)  s"  " pocket (prepend$)  bl parse pocket (prepend$)
    pocket count evaluate ;
[then]

[defined] config-sys-sf [if]

\ intent: substitute one word with another
\ usage: subst <old> <new>
  aka aka subst  ( - <old> <new> )  \ substitute one word with another
[then]
