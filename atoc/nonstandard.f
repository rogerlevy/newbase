\ win32/Linux compatibility

create pad 1024 allot \ redefine pad to hold up to 1024 bytes

\ intent: gets the number of items on datastack
\ usage: s.depth
subst depth s.depth  ( - n )  \ gets the number of items on datastack

\ intent: puts a counted string on the datastack
\ usage: " text goes here"
subst s" " immediate  \ an alias for s"

\ intent: speeding up some often-used short routines
\ usage: macro:  <some code> ;  \ entire declaration must be a one-liner!
: macro:  ( - <code> ; )  \ define a macro; the given string will be evaluated when called
  create immediate
  [char] ; parse string,
  does> count evaluate ;

\ intent: enter compile mode and compile a literal
\ usage: : foobar [ 1 2 + ]# ; \ when foobar executes, 3 will be placed on the datastack
: ]#  ( - n )  \ enter compile mode and compile a literal
  ] postpone literal ;

\ intent: converts number of bytes to number of cells
\ usage: 256 cell/
: cell/  ( n - n/cell )  \ convert # of bytes to # of cells
  cell / ;

\ intent: define named value only if it does not already exist
\ usage: <n> ?value <name>
: ?value  ( n - <name> )  \ if value doesn't exist, create it.  if it does, change it.
  >in @ >r
  absent if r> >in !  value
  else
    r> >in !  ' >body !
  then ;

\ intent: define named constant only if it does not already exist
\ usage: <n> ?constant <name>
: ?constant  ( n - <name> )  \ if constant doesn't exist, create it.  otherwise do nothing.
  >in @ >r
  absent if
    r> >in ! constant
  else
    r> drop drop
  then ;

\ intent: comma a string into the dictionary
\ usage: <string> string,
: string,  ( adr c - )  \ comma a string (fixed sf bug where they had a zstring)
  string, -1 allot ;

\ intent: get execution token of the last word defined
\ usage: last-xt
: last-xt  ( - xt )  \ get xt of last word defined
  last @ name> ;

\ intent: define chunk of code as a string which will be executed when given <name> is executed.
\ usage: #define <name> <code>
: #define  ( - <name> <code> )  \ fake preprocessor code definition
  create 0 parse bl skip string, does> count evaluate ;

\ intent: statically compile large chunks of data into the dictionary
\ usage: <adr> <count> copy,
: copy,  ( adr c - )  \ compile given data into dictionary
  ?dup if  here swap dup allot move  else  drop  then ;

\ intent: duplicate the second item on the stack in place
\ usage: 1 2 udup \ results in 1 1 2 on the stack
: udup  ( a b - a a b )  \ duplicate the second item on the stack in place
  over swap ;

\ intent: calculate the number of bytes in given number of mebibytes
\ usage: 1 megs \ results in 1048576 on the stack
: megs  ( n - n*1024*1024 )  \ calculate the number of bytes in given number of mebibytes
  1024 * 1024 * ;

cd nonstandard
include extended-primitives
include psuedo-closures
include string-operations
include reporting
include linked-list
include order-stack
include directory-utilities
include fload
include sessions
cd ..