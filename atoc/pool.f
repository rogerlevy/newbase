\ ==============================================================================
\ Object Pools
\ ========================= copyright 2014 Roger Levy ==========================

0 value freestack   \ the free object pool
0 value theclass    \ the class of the pool
0 value (pool)      \ holds reference to pool being reset
0 value (freestack) \ holds reference to freestack being reset

\ intent: create a named pool of given object class which holds given count elements
\ usage: <count> <object class> pool <name>
: pool  ( n class - <name> ) ( - free-stack )  \ create object pool
  to theclass  locals| n |
  create
    here to freestack  0 ,
    n theclass sizeof array,
    here freestack !  n stack,
    [[ theclass over initialize  freestack @ push ]] freestack cell+ each
  does> cell+ ;

\ intent: create a named pool of data of given size and given count elements
\ usage: <count> <size> datapool <name>
: datapool  ( count size - <name> ) ( - array )  \ create data pool
  locals| size n |
  create
    here to freestack  0 ,
    n size array,
    here freestack !  n stack,
    [[ freestack @ push ]] freestack cell+ each
  does> cell+ ;

\ intent: obtain an uninitialized object from the pool
\ usage: <pool> pool-one
: pool-one  ( pool - item )  \ get object from pool
  cell- @ pop ;

\ intent: release object back into pool
\ usage: <item> <pool> pool-return
: pool-return  ( item pool - )  \ put object back in pool
  cell- @ push ;

\ intent: resets the object pool
\ usage: <pool> reset-pool
: reset-pool  ( pool - )  \ reset pool
  to (pool)  (pool) cell- @ to (freestack)
  (freestack) vacate  [[ (freestack) push ]] (pool) each ;

\ intent: check to see if the given pool has the given count of free objects available
\ usage: <count> <pool> pool-free?
: pool-free?  ( n pool - flag )  \ check if the pool has enough free objects
  cell- @ length <= ;
