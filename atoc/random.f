\ ==============================================================================
\ ForestLib
\ Random number generator
\ ========================= copyright 2014 Roger Levy ==========================

variable bud \ random number seed.

\ intent: a factor of RND, but can be used alone.
\ usage: rand
: rand  ( - u )  \ generate random 32-bit number.
  bud @  3141592621 *  1+  dup bud ! ;

\ usage: <n> rnd
: rnd   ( n - u )  \ generate random number between 0 and n.
  rand um* nip ;

\ intent: meant to be used at the start of your program to ensure you get a new stream of random numbers.
\ usage: /rnd
: /rnd  ( - )  \ initialize random number generator based on system counter.
  dcounter drop bud ! ;

\ initialize random number generator.
/rnd
