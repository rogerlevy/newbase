
\ intent: prints the floating point variable to stdout
\ usage: myfloat f?
: f?  ( adr - )  \ print float variable
  f@ f. ;

\ intent: convert float to 32-bit signed integer
\ usage: myfloat f>s
subst (f>s) f>s  ( - n f: r - ) \ disables rounding.  this fixes random crashes.

\ intent: originally for fetching packed rgba8888 values.
\ usage: address 4c@f
: 4c@f   ( adr - f: n n n n )  \ fetch 4 chars as floats (0~255 -> 0~0.1)
  a!>  c@+ c>f  c@+ c>f  c@+ c>f  c@+ c>f ;

\ this always crashes for me. Maybe I am doing something wrong. I don't know...
\ intent: originally for fetching from float source rectangles for drawing allegro sprites
\ usage:
: 4sf@   ( adr - f: n n n n )  \ fetch 4 32-bit floats
  a!>  sf@+ sf@+ sf@+ sf@+ ;

\ intent: for passing source rectangles directly to allegro
\ usage: 1 2 3 4 4s>f
: 4s>f   ( n n n n - f: - r r r r )  \ convert 4 ints to floats
  >r >r  swap s>f s>f  r> s>f  r> s>f  ;
