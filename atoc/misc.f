#undefined config-arch-x86  [if]
  cr  .( misc.f load error: unsupported architecture - only x86 at the moment. )
  quit
[then]

\ -------------------------------------------------------------------------------------------------
\ FOR/NEXT

\ not really recommended for general use, it's a little slower than DO/LOOP, surprisingly...
\ the only place I use it is in game entity scripts since it's light on return stack usage

\ intent: create a simple construct for flow-control (only within colon-definitions or closure)
\ usage: <limit> for <code> next
: for  ( n - )
  1 postpone literal  postpone -  postpone >r  here  postpone noop ( the noop thwarts optimizer ) ; immediate

: next  ( - )
  [defined] config-sys-vfx [if]
    [+asm]  0 [esp] dec  jns  [-asm]  postpone r>drop ; immediate
  [then]
  [defined] config-sys-sf [if]
    [+asm]  dec  0 [esp]  jns  [-asm]  postpone r>drop ; immediate
  [then]


\ -------------------------------------------------------------------------------------------------
\ Memory operators

\ intent: fetch a double-precision value from given address
\ usage: <addr> d@
subst 2@ d@  ( addr - d )  \ Fetch double

\ intent: store a double-precision value to given address
\ usage: <double> <addr> 2!
subst 2! d!  ( d addr - )  \ Store double

\ intent: define a double-precision variable
\ usage: dvariable <name>
subst 2variable dvariable ( - <name> )  \ Define double variable

\ glossary for included memory operators below:

\ intent: store 2D vector to given address
\ usage: <v1> <addr> 2!
\ dox:  2!  ( x y addr - )  \ Store x,y (keep x,y order in memory)

\ intent: fetch 2D vector from given address
\ usage: <addr> 2@
\ dox:  2@  ( addr - x y )  \ Fetch x,y

\ intent: add 2D vector to 2D vector found at given address
\ usage: <v1> <addr> 2+!
\ dox:  2+!  ( x y addr - )  \ Add-store x,y

\ intent: add double-precision value to value found at given address
\ usage: <double> <addr> d+!
\ dox:  d+!  ( d addr - )  \ Add-store double

\ intent: store 3D vector to given address
\ usage: <v1> 3!
\ dox:  3!  ( x y z addr - )  \ Store x,y,z

\ intent: fetch 3D vector from given address
\ usage: <addr> 3@
\ dox:  3@  ( addr - x y z )  \ Fetch x,y,z

\ intent: add 3D vector to 3D vector found at given address
\ usage: <v1> 3+!
\ dox:  3+!  ( x y z addr - )  \ Plus-store x,y,z

#defined config-sys-sf  #defined config-arch-x86  and  [if]
  include dialects/sf-x86-memory-ops.f
[else]
  include dialects/forth94-memory-ops.f
[then]

\ intent: add given single integer to double found at given address. used for high-resolution counters etc
\ usage: <int> <addr> m+!
: m+!  ( n adr - )  \ add a cell int to a double
  dup >r d@ rot m+ r> d! ;

\ -------------------------------------------------------------------------------------------------
\ Coordinate operators

\ 2+ and 2- are obsolete Forth words.  I've redefined them to something more useful.

\ glossary for included coordinate operators below:

\ intent: add two 2D vectors together
\ usage: <v1> <v2> 2+
\ dox: 2+   ( x y x y - x y )  \ add 2D vectors

\ intent: subtract one 2D vector from another 2D vector
\ usage: <v1> <v2> 2-
\ dox: 2-  ( x y x y - x y )  \ subtract 2D vectors

\ intent: add two 3D vectors together
\ usage: <v1> <v2> 3+
\ dox: 3+  ( x y z x y z - x y z )  \ add 3D vectors

\ intent: subtract one 3D vector from another 3D vector
\ usage: <v1> <v2> 3-
\ dox: 3-  ( x y z x y z - x y z ) \ subtract 3D vectors

\ intent: swap X component of two vectors
\ usage: <v1> <v2> xswap
\ dox: xswap  ( x1 y1 x2 y2 - x2 y1 x1 y2 )  \ vector X swap

\ intent: swap Y component of two vectors
\ usage: <v1> <v2> yswap
\ dox: yswap  ( x1 y1 x2 y2 - x1 y2 x2 y1 )  \ vector Y swap

\ intent: convert rectangle from position size to left top right bottom format
\ usage: <xywh> area
\ dox: area  ( x y w h - x1 y1 x2 y2 )  \ convert xywh to ltrb

\ intent: convert rectangle from left top right bottom format to position size format
\ usage: <ltrb> -area
\ dox: -area  ( x1 y1 x2 y2 - x y w h )  \ convert ltrb to xywh

#defined config-sys-sf  #defined config-arch-x86  and  [if]
  include dialects/sf-x86-coordinate-ops.f
[else]
  include dialects/forth94-coordinate-ops.f
[then]

\ -------------------------------------------------------------------------------------------------
\ Profiling


\ intent: get the duration of execution for a given word's xt in microseconds
\ usage: <xt> exectime
: exectime  ( xt - n )  \ execution time of xt
  ucounter 2>r  execute  ucounter 2r> d-  d>s ;

\ intent: print the execution time of a given xt
\ usage: <xt> time?
: time?  ( xt - )  \ print time given XT takes in microseconds
  exectime . ;

\ -------------------------------------------------------------------------------------------------
\ Commandline extension: a nicer DIR

PACKAGE SHELL-TOOLS

\ intent: gives file or directory name based on file attribute
\ usage: {not used externally}
: .FOUNDNAME  ( addr - )  \ file or directory name
  DUP @ FILE_ATTRIBUTE_DIRECTORY AND IF
    ZDIRNAME
  ELSE
    ZFILENAME
  THEN
  ?TYPE CR ;

PUBLIC

\ intent: pretty-print directory listing of current directory
\ usage: dir
: DIR  ( - )  \ pretty-print directory listing
  pwd CR
  FILESPEC PAD FindFirstFile >R
  R@ INVALID_HANDLE_VALUE <>
  BEGIN ( flag) WHILE
    PAD .FOUNDNAME
    R@ PAD FindNextFile ( flag)
  REPEAT R> FindClose DROP ;

END-PACKAGE

\ ------------------------------------------------------------------------------

\ intent: cleans up a given path by stripping trailing spaces and converting path separators to portable format.
\ usage: <path> processpath
: processpath  ( adr c - adr c )  \ strip trailing spaces and convert \'s to /'s
  bl skip -trailing  2dup [char] / [char] \ REPLACE-CHAR ;

\ intent: read path string
\ usage: namespace <path> |
: namespec  ( - <path> | ... )  \ read path from input stream terminated by a | (pipe)
  [char] | parse processpath ;

\ intent: needed in some cases to avoid bugs introduced from name collisions, or to
\ intent: disallow the use of a word.
\ usage: smudge <name>
: smudge  ( - <word> )  \ disable a word from being searchable in the dictionary.
  defined if  >name 1 erase  else  drop  then ;

\ intent: The DOES> component of :XT
\ usage: not intended to be used outside of this file
: do-:const  ( - )  \ does fetch
  does> @ ;

\ intent: for creating words that are not meant to be directly called and save the programmer having to put in a ' or a ['].
\ usage: :xt <name> <code> ;
: :xt  ( - <word> <code> ; )  \ creates a word that puts its xt on stack instead of executing when called.
  create  do-:const  here 0 ,  :noname swap ! ;
