\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ ==============================================================================
\ Newbase "A Touch Of Class" Primary Toolbelt Loader
\ ========================= copyright 2014 Roger Levy ==========================

include subst   \ word for aliasing words
include defined \ bring in the word checking words #defined #undefined

#undefined x86 [if]  true constant x86  [then]

include pathtools \ miscellaneous patches to sf provided filepath words

cd qfolder
include qfolder \ a collection of directory-walking words
cd ..

true constant HW-FLOAT-STACK
include fpmath  \ note this version adds fixed point recognition to the interpreter

include new-include \ modified version of SwiftForth's INCLUDE that supports disabling cross-reference data
include nonstandard \ so-called because it changes 2+ , @+ , pushpath , 2@ , and more.

include imove \ faster move working with cells instead of bytes

include random \ pseudorandom number generator
include misc \ FFI utilities, for/next, extended load/store words, extended arithmetic, performance measurement, psuedo-closures ( [[ ]] )
include fpext \ floating point extensions for FFI
include a-register \ the A register
include fixedp \ fixed-point operators
include numbers \ number conversions and utility routines

cd prototype
include prototype \ OOP system
cd ..

include regions \ region-based memory management
include pool \ object pooling

pushpath cd ../.. !root poppath
pushpath          !home poppath

report( newbase: Loaded "A Touch Of Class" Toolbelt )
