\ ==============================================================================
\ ForestLib
\ Fixed point (16.16) math and printing
\ ========================= copyright 2014 Roger Levy ==========================

[defined] config-arch-x86  [defined] config-sys-sf  and  [if]
  fload sf-x86-fixedp
[else]
  fload forth94-fixedp
[then]

\ intent: integer quad to fixed-point quad conversion
\ usage: <quad> 4s>p
: 4s>p  ( a b c d - a. b. c. d. )  \ convert four ints to fixed
  s>p >r 3s>p r> ;

\ intent: fixed-point quad to integer quad conversion
\ usage: <quad.> 4p>s
: 4p>s  ( a. b. c. d. - a b c d )  \ convert four fixed to ints
  p>s >r 3p>s r> ;

\ intent: fixed-point to character conversion
\ usage: <n.> p>c
: p>c  ( n. - c )  \ convert from 0...1.0 (fixed) to 0...255 (byte)
  dup if 1 - then $ffff and  8 >> ;

\ intent: character to fixed-point conversion
\ usage: <character> c>p
: c>p  ( c - n. )  \ convert from 0...255 (byte) to 0...1.0 (fixed)
  8 <<  $10000 $ff00 */ ;

\ fixed-point conversion optimization ~ 1.33x faster execution
65536e fconstant 65536e
1e 65536e f/ fconstant 1/65536e

\ intent: fixed-point to float conversion
\ usage: <n.> p>f
: p>f  ( n. - f: n )  \ convert fixed to float
  s>f 1/65536E f* ;

\ intent: float to fixed-point conversion
\ usage: <float> f>p
: f>p  ( f: n - n. )  \ convert float to fixed
  65536E f* (f>s) ;

pi f>p constant pi.              \ fixed-point PI
pi/2 f>p constant pi/2.          \ fixed-point PI / 2
pi/180 f>p constant pi/180.      \ fixed-point PI / 180
180e pi f/ f>p constant 180/ppi. \ fixed-point 180 / PI

\ intent: multiply two fixed-point values
\ usage: <n1.> <n2.> p*
subst fixed-mul p*  ( n. n. - n. )  \ fixed-point multiply

\ intent: divide two fixed-point values (floating-point division used)
\ usage: <n1.> <n2.> p/
: p/  ( n. n. - n. )  \ fixed-point divide
  swap p>f p>f f/ f>p ;

\ intent: fixed-point cosine (floating-point cosine used)
\ usage: <degrees.> pcos
: pcos  ( deg. - n. )  \ fixed cosine
  p>f cos f>p ;

\ intent: fixed-point sine (floating-point sine used)
\ usage: <degrees.> psin
: psin  ( deg. - n. )  \ fixed sine
  p>f sin f>p ;

\ intent: linear interpolation between two values
\ usage: <start> <end> <time> interp
: lerp  ( src dest n. -- )  \ interpolate.  factor is on a scale from 0 ~ 1.0
  >r over - r> p* + ;

\ intent: useful for calculating progress or having one value influence another one within certain bounds.
\ usage: <value> <range min a> <range max a> <range min b> <range max b> rescale
: rescale  ( n|. min1|. max1|. min2|. max2|. - n|. )  \ transform a number from one range to another.
  locals| max2 min2 max1 min1 n |
  n min1 -  max1 min1 -  p/  max2 min2 -  p*  min2 + ;

\ intent: fixed-point square-root function (uses floating-point sqrt)
\ usage: <n.> psqrt
: psqrt  ( n. - n. )  \ fixed square root
  p>f fsqrt f>p ;

\ intent: pfloor two fixed-point values
\ usage: <v1.> 2pfloor
: 2pfloor  ( x. y. - x. y. )  \ fixed floor a 2D vector
  pfloor >r pfloor r> ;

\ intent: pfloor three fixed-point values
\ usage: <v1.> 3pfloor
: 3floor  ( x. y. z. - x. y. z. )  \ fixed floor a 3D vector
  pfloor >r  pfloor >r  pfloor  r> r> ;

\ intent: print fixed-point value (uses floating-point print)
\ usage: <n.> p.
: p.  ( n. - )  \ fixed print
  p>f f. ;

\ intent: print fixed-point 2D vector
\ usage: <v1.> 2p.
: 2p.  ( x. y. - )  \ fixed 2D vector print
  swap p. p. ;

\ intent: print fixed-point 3D vector
\ usage: <v1.> 3p.
: 3p.  ( x. y. z. - )  \ fixed 3D vector print
  rot p. swap p. p. ;

\ intent: fetch from address and print result as fixed-point value
\ usage: <addr> p?
: p?  ( adr - )  \ fetch and fixed print
  @ p. ;

\ intent: fetch 2D vector from address and print result as fixed-point 2D vector
\ usage: <addr> 2p?
: 2p?  ( adr - )  \ two-fetch and fixed 2D vector print
  2@ 2p. ;

\ intent: fetch 3D vector from address and print result as fixed-point 3D vector
\ usage: <addr> 3p?
: 3p?  ( adr - )  \ three-fetch and fixed 3D vector print
  3@ 3p. ;

\ intent: fetch quad from address and print result as fixed-point quad
\ usage: >addr? 4p?
: 4p?  ( adr - )  \ four-fetch and fixed quad print
  dup 2p? 2 cells + 2p? ;

\ intent: fixed-point 2D vector to floating-point 2D vector conversion
\ usage: <v1.> 2p>f
: 2p>f  ( x. y. - f: x y )  \ convert fixed 2D vector to float 2D vector
  swap p>f p>f ;

\ intent: fixed-point 3D vector to floating-point 3D vector conversion
\ usage: <v1.> 3p>f
: 3p>f  ( x. y. - )  \ convert fixed 3D vector to float 3D vector
  >r >r  p>f  r> p>f  r> p>f ;

\ intent: fixed-point quad to floating-point quad conversion
\ usage: <quad.> 4p>f
: 4p>f  ( a. b. c. d. - f: a b c d )  \ convert fixed quad to float quad
  >r >r  swap p>f  p>f  r> p>f  r> p>f  ;

\ intent: floating-point 2D to fixed-point 2D vector conversion
\ usage: <f:v1> 2f>p
: 2f>p  ( f: x y - )  ( s: - x. y. )  \ convert float 2D vector to fixed 2D vector
  f>p f>p swap ;

\ intent: fetch four fixed-point values from given address and place them on the F-stack
\ usage: <addr> 4p@f
: 4p@f  ( adr - f: a. b. c. d. )  \ fetch four floats from address to F-stack
  a!>  @+ p>f  @+ p>f  @+ p>f  @+ p>f ;

\ intent: floating-point quad to fixed-point quad conversion
\ usage: <f:quad> 4f>p
: 4f>p  ( f: a b c d - )  ( s: - a. b. c. d )  \ convert float quad to fixed quad
  2f>p 2f>p 2swap ;

\ conditional conversion (provides some fake type-inference)
\ used to make "smart" words
\ depending on the expected input range it lets words accept integers or fixed point

\ intent: {_INTENT}
\ usage: {_USAGE}
: ?p  ( n|. - n. )  \ {_BRIEF}
  dup abs 32768 <= if  s>p  then ;

\ intent: {_INTENT}
\ usage: {_USAGE}
: ?ps  ( n|. - n. )  \ {_BRIEF}
  dup abs 360 <= if  s>p  then ;

\ intent: {_INTENT}
\ usage: {_USAGE}
: 2?p  ( x|. y|. - x. y. )  \ {_BRIEF}
  ?p swap ?p swap ;

\ intent: {_INTENT}
\ usage: {_USAGE}
: 2?ps  ( x|. y|. - x. y. )  \ {_BRIEF}
  ?ps swap ?ps swap ;

\ intent: {_INTENT}
\ usage: {_USAGE}
: 4?p  ( a|. b|. c|. d|. - a. b. c. d. )  \ {_BRIEF}
  2?p 2swap 2?p 2swap ;
