
\ -------------------------------------------------------------------------------------------------
\ Callback fix  (SwiftForth)

windows-interface open-package
public

\ intent: define a callback passable to the target system's external libraries.
\ usage: <xt> <callback id> cb <name>
: cb:  ( xt n - <name> )  \ define callback for external library
  create
  runcb ,call dup
  [+asm] cells # ret nop [-asm]
  0= if [+asm] nop nop [-asm] then  \ fix bug in 3.0.6 & 7
  ( xt) , ;
end-package
