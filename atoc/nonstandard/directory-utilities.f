
\ -------------------------------------------------------------------------------------------------
\ Directory utility classes

\ intent: put the current directory on the datastack as a counted string
\ usage: pad 256 over getdir \ results in counted string - use type to print out
: getdir  ( count adr - adr c )  \ generic fetch directory
  [defined] GetCurrentDirectory [if]
    GetCurrentDirectory
  [else]
    swap getcwd zcount
  [then] ;

\ intent: changes the current directory to the given directory
\ usage: z" directory" setdir
\ note: leaves a 1 on the datastack if the path exists, 0 if not.
: setdir  ( zadr - n )  \ generic set directory
  [defined] SetCurrentDirectory [if]
    SetCurrentDirectory
  [else]
    chdir
  [then] ;

subst getdir dir@ immediate  \ alias for getdir
subst setdir dir! immediate  \ alias for setdir

create dirstack 2048 cell+ /allot  \ improved path stack ... is a circular buffer; includes offset into itself at beginning

\ intent: get current address in dirstack
\ usage: dirstack>
: dirstack>  ( - adr )  \ fetch address from dirstack
  dirstack cell+ dirstack @ 2047 and + ;

\ intent: push current directory onto dirstack
\ usage: pushpath
: pushpath  ( - )  \ push current path
  255 dirstack> getdir drop
  256 dirstack +! ;

\ intent: get the last path pushed to dirstack
\ usage: lastpath
: lastpath  ( - adr c )  \ get previous path
  -256 dirstack +!  dirstack> zcount  256 dirstack +! ;

\ intent: pop off the last path (and cd into it)
\ usage: poppath
: poppath  ( - )  \ pop path and go to it
  -256 dirstack +!
  dirstack> setdir drop ;

\ intent: I can't fathom a single use for this word other than creating complexity.
\ usage: Don't
: droppath  ( - )  \ redefined to throw an error
  -1 abort" DROPPATH? WTF???" ;
