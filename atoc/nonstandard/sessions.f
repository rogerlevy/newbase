
\ ------------------------------------------------------------------------------
\ Sessions

create session$ 256 allot \ holds current session filename

\ intent: declare the current session
\ usage: session <name>
: session  ( - <name> )  \ declare current session.  the file this is declared in will be used by RLD .
  create
  !home pushpath
  including session$ place
  ; \ cr inverse session$ count -name [char] \ scanlast type normal cr ;

\ intent: quick reloading of code without having to restart SwiftForth.
\ usage: rld
: rld  ( - )  \ reload the session file
  " empty" evaluate  \ make sure we use the newest one
  zhome setdir drop
  session$ count included ;
