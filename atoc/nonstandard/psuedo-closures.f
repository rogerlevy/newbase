
\ -------------------------------------------------------------------------------------------------
\ Psuedo-closures

\ the way this works is very "clever"
\ i don't want to document all of these in detail and would rather treat this stuff
\ as black box for now

: does-:[  IMMEDIATE DOES> @  ( xt )  compile,  postpone (else) here +bal  false ;

: >quote  5 + code> ;

: :[]  ( - <name> )
  CREATE here 0 , does-:[
  :NONAME swap ! postpone r@ postpone >quote ;

: ]]  ( - )  \ end psuedo closure, see [[
  if postpone noop ( thwart some nasty bugs ) postpone ; exit then
  postpone exit  >resolve ; IMMEDIATE

subst ]] ;] IMMEDIATE
:[] [[  ( - xt ) ;

\ intent: for passing XT's to other functions without having to define new words.
\ usage: [[ <code> ]] ( xt )
: [[  ( - xt )  \ start psuedo closure; can be used in or out of colon defs.
  state @ if postpone [[ else :noname true then ; IMMEDIATE
