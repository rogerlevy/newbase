\ {nodoc} tell the documentation progress monitor tool to ignore this file

\ -------------------------------------------------------------------------------------------------
\ Enhanced include facility - FLOAD
\  Features:
\  If only a filename is given, FLOAD automatically searches the directory structure for the file, recursively,
\  starting with the current folder, then if it isn't found, from the directory of THIS file, by default.
\  To change the root folder for searching, use ROOT!
\  Automatic saving and restoring of search order using circular buffer, to relieve user of need to keep track of it
\  Moves the current directory to the given source file, as expected.  Restores it after loading it or if there was an error.
\  4/27/13 after checking the current folder and subdirs, it checks the new "home" folder's direct child folders
\        before checking the "root" recursively.

create zroot 256 /allot  \ "project" root folder
create zhome 256 /allot  \ "work" root folder
create efolder 256 /allot  \ exclude from search
\ create zfolder 256 /allot


\ intent: {INTENT}
\ usage: {USAGE}
: !root ( - )  \ {BRIEF}
  256 zroot getdir drop ; !root  \ set root folder to current folder

\ intent: {INTENT}
\ usage: {USAGE}
: !home ( - )  \ {BRIEF}
  256 zhome getdir drop ; !home  \ set home folder to current folder

\ intent: {INTENT}
\ usage: {USAGE}
: default.ext  ( adr c - adr c )  \ append .f to string if no extension given
  2dup [char] . skip " ." search nip nip 0= if <$ " .f" $+ $> then ;


\ file stack: allows us to use QUIT to break compilation without leaving a bunch of files locked.
variable reloading?
variable fnest
create files  4096 allot
variable >files

\ intent: {INTENT}
\ usage: {USAGE}
: >file  ( adr c - )  \ {BRIEF}
  >files @ files + place  >files @ 256 + 4095 and >files ! ;

\ intent: {INTENT}
\ usage: {USAGE}
: file>  ( - adr c )  \ {BRIEF}
  >files @ 256 - 4095 and >files !  >files @ files + count ;

\ intent: this redefinition of QUIT allows scripts to use QUIT to stay in the current directory or for debugging
\ usage: {USAGE}
: quit  ( - )  \ stop everything and return to the interactive interpreter
  'source-id @ 0 > if  'source-id @ close-file drop  then  quit ;

\ intent: {INTENT}
\ usage: {USAGE}
: ((fload))  ( path c  filename c - )  \ {BRIEF}
  2swap zfolder zplace
  pushpath push-order
    zfolder setdir drop
    -path ['] included catch ?dup if  fnest --  throw  then
  poppath pop-order
;

\ intent: {INTENT}
\ usage: {USAGE}
: (fload)  ( addr c - )  \ {BRIEF}
  'source-id @ 0 > if
    including >file 'source-id @ close-file drop
    ((fload))
    file> r/o open-file drop 'source-id !
  else
    ((fload))
  then
;

\ intent: {INTENT}
\ usage: {USAGE}
: dot...folder  ( addr c -- flag )  \ {BRIEF}
  1 >  swap c@ [char] . = and ;

\ intent: {INTENT}
\ usage: {USAGE}
: exclude-dot...folder-filter  ( zfolderpath -- enter? )  \ {BRIEF}
  zcount dot...folder not ;

\ intent: {INTENT}
\ usage: {USAGE}
: exclude-folder-filter  ( zfolderpath -- enter? )  \ {BRIEF}
  zcount
  2dup 2>r  efolder zcount uppcompare 0=
  2r>  dot...folder or not
;

\ intent: {INTENT}
\ usage: {USAGE}
: $fload  ( filespec c - )  \ {BRIEF}
  /bal
  fnest ++
  r-buf
  default.ext r@ zplace

  \ cr pwd
  0 efolder c!
  " ." zfolder zplace
  zfolder setdir drop
  r@ ['] exclude-dot...folder-filter ?folder-fast2 if r> zcount (fload)  fnest --  exit then

  \ ." home"
  256 efolder getdir drop
  zhome zcount zfolder zplace
  zfolder setdir drop
  r@ ['] exclude-folder-filter ?folder-fast2 if r> zcount (fload)  fnest --  exit then

  \ ." root"
  zhome zcount efolder zplace
  zroot zcount zfolder zplace
  zfolder setdir drop
  r@ ['] exclude-folder-filter ?folder-fast2 if r> zcount (fload)  fnest --  exit then

  0 fnest !
  r> zcount here place  -38 throw
;

\ intent: {INTENT}
\ usage: FLOAD file
\ usage: FLOAD file.f
\ usage: FLOAD path/to/file
\ usage: FLOAD path/to/file.f
: fload  ( -- <filespec> )  \ Include a file that's in an unknown folder.  search order:  1) current 2) subfolders 3) from home and subfolders excluding current and 4) from root
  pushpath  bl word count $fload  poppath ;

\ intent: {INTENT}
\ usage: {USAGE}
: ?fload  ( flag - <filespec> )  \ conditionally load a file
  if  fload  else  bl word drop  then ;

\ intent: prepend the home path if given path is not found
\ usage: <path> ?homepath
: ?homepath  ( path c -- path c )  \ prepend home path if given filepath is not found
  2dup file-exists ?exit  zhome zcount <$ " \" $+ $+ $>  ;
