
\ ------------------------------------------------------------------------------
\ string concatenation
\ NOTE: this is not implemented the best way and is probably due for a rehaul

create $buffers  16384 allot  \ string concatenation buffer stack (circular)
variable >$                   \ pointer into $buffers

\ intent: begin a builder-string
\ usage: <string> <$
: <$  ( adr c - )  \ begin a string.  NOTE: this component not implemented the best way and is due for a rehaul!
  >$ @ 256 + 16383 and >$ !  >$ @ $buffers + place ;

\ intent: concatenate string to current builder-string (call <$ first)
\ usage: <string> $+
: $+  ( adr c - )  \ append a string to current string
  >$ @ $buffers + append ;

\ intent: concatenate a character to current builder-string (call <$ first)
\ usage: <character> c$+
: c$+  ( c - )  \ append a char to current string
  >$ @ $buffers + count + c!  1 >$ @ $buffers + c+! ;

\ intent: finish building the string and fetch the finished counted string  (call <$ then $+ or c$+ first)
\ usage: $>
: $>  ( - adr c )  \ fetch finished string
  >$ @ $buffers + count  ( >$ @ 256 - 16383 and >$ ! ) ;

\ MINI STRING-BUILDING TUTORIAL
\ If you want to build the string " This is a test" from the four words
\ You can use the following example code
: mini-string-building-tutorial  ( - )  \ example code for string building
  " This" <$  \ start builder-string
  "  is" $+   \ append to builder-string
  "  a" $+    \ append to builder-string
  "  test" $+ \ append to builder-string
  $>          \ finish building the string and fetch the finished counted string
  ;

\ intent: counted string to zstring conversion
\ usage: <string> z$
: z$  ( addr c - zaddr )  \ convert string to zero-terminated string
  >$ @ $buffers + zplace  >$ @ $buffers +  >$ @ 256 + 16383 and >$ ! ;

\ intent: append a character to the end of a counted string
\ usage: <character> <addr> cappend
: cappend  ( c adr - )  \ append char
  dup >r  count + c!  1 r> c+! ;

\ intent: reverse the count operation on a counted string
\ usage: <string> uncount
: uncount  ( adr c - adr-1 )  \ 'undo' the count word's operation
  drop 1 - ;

\ intent: concatenate two strings
\ usage: <string1> <string2> strjoin
: strjoin  ( first c second c - first+second c )  \ concat strings
  2swap <$ $+ $> ;

\ intent: provide a means to get string input from the user
\ usage: <buffer> <size> input
: input  ( adr c - )  \ get a string from the user
  over 1 +  swap accept  swap  c! ;
