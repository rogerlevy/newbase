
\ ------------------------------------------------------------------------------
\ statically compiled forwards-linked-lists; primarily used in spriter-schema.f atm

0 value xt \ holds xt while iterating through list

\ intent: potentially-reusable factor for LIST
: list,  ( - addr )  \ compile header for a list
  here  ( first ) 0 , ( last ) 0 , ( count ) 0 , here here third 2!
  ( link-addr ) ( next ) 0 , ( data ) 0 , ;

5 cells constant /list  \ size of list header

\ intent: create a named linked-list
\ usage: list <name>
: list  ( - <name> )  \ declare a list
  create list, drop ;

\ intent: add given item to the given list
\ usage: <item> <list> link,
: link,  ( item list - )  \ add an item to a list; uses dictionary
  swap >r  dup cell+ @  here   0 , r> ,  locals| thislink lastlink list |
  thislink lastlink !  thislink list cell+ !   1 list cell+ cell+ +! ;

\ intent: create a circular list by linking the given item to the first item in the list
\ usage: <item> <list> clink,
: clink,  ( item list - )  \ add an item to a list, linking it back to the first item to make it circular; uses dictionary
  dup >r  link,  r@ @ r> cell+ @ ! ;

\ intent: get the number of items in a list
\ usage: <list> howmany
: howmany  ( list - count )  \ get # of items in a list
  cell+ cell+ @ ;

\ intent: call given xt on given list for each item in the list (xt should take in an item and return nothing)
\ usage: <xt> <list> traverse
: traverse  ( xt list - )  ( item - )  \ itterate on a list; warning do not use with circular lists
  xt >r  swap to xt
  @ ( skip first dummy link )
  begin @ dup while dup >r  cell+ @ xt execute  r> repeat
  drop  r> to xt ;

\ intent: reset a list
\ usage: <list> startover
: startover  ( list - )  \ reset a list.
  dup cell+ cell+ off dup dup @ swap cell+ ! @ 2 cells erase ;
