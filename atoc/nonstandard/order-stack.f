
\ -------------------------------------------------------------------------------------------------
\ "A" register
\ This stuff is in a wordlist to ensure that things don't conflict with SwiftForth's @+ !+ etc.
\ Later I redefine them in the FORTH wordlist because it's almost guaranteed we won't
\ be loading any SwiftForth code after ForestLib is loaded.

wordlist constant [a]
[a] +order definitions
variable 'a

\ usage: a!>  @+ ...  @+ ...
: @+  ( - n )  \ fetch-plus: fetch cell from address in A and increment A by one cell
  'a @ @  cell 'a +! ;
\ usage: a!>  1 !+  2 !+  3 !+
: !+  ( n - )  \ store-plus: store cell to address in A and increment A by one cell
  'a @ !  cell 'a +! ;
: !a  ( n - )  \ store-a: store cell at address in A
  'a @ ! ;
: @a  ( - n )  \ fetch-a: fetch cell at address in A
  'a @ @ ;
: a!>  ( adr - )  \ a-store-for: store address in A for remainder of definition (auto-restored on return)
  r>  'a @ >r  swap 'a !  call  r> 'a ! ;

previous definitions
[a] +order

\ order stack
20 constant #orders  \ order stack depth
20 cells constant /order  \ # of cells per order entry
create orders /order #orders * /allot  \ order stack (circular)
variable o   \ index of the next "order" in the circular buffer
variable 'm  \ contains voc for 'protected' words
variable 'm2  \ contains voc for 'exposed' words

\ intent: get current address in order stack
\ usage: order>
: order>  ( - adr )  \ get current address in order stack
  o @ /order * orders + ;

\ intent: increments order stack index using boundary wrapping for circular placement
\ usage: 1 +o
: +o  ( n - )  \ increment order stack "index"
  o @ + 0 #orders wrap o ! ;

\ intent: places a new search order on the order stack
\ usage: push-order
: push-order  ( - )  \ places a new search order on the order stack
  order>  a!>  get-order !a  @a reverse @+  0 ?do  !+  loop  get-current !+  'm @ !+  'm2 @ !+
  1 +o ;

\ intent: removes the top item from the order stack and sets the current search order
\ usage: pop-order
: pop-order  ( - )  \ removes the top item from the order stack and sets the current search order
  -1 +o
  order>  a!>  @+ dup >r  0 ?do  @+  loop  r> set-order @+  set-current  @+ 'm !  @+ 'm2 ! ;

[a] -order
