
\ -------------------------------------------------------------------------------------------------
\ Extended primitives

\ intent: decrement operator
\ usage: <adr> --
: --  ( adr - )  \ decrement cell at address
  -1 swap +! ;

\ intent: toggles flag at given address between true and false
\ usage: <adr> toggle
: toggle  ( adr - )  \ toggle a flag. (0 <-> -1)
  dup @ not swap ! ;

\ intent: negate the top two numbers on the datastack
\ usage: 5 8 2negate \ results in -5 -8 on the stack
: 2negate  ( x y - -x -y )  \ negate 2 numbers
  negate swap negate swap ;

\ intent: adds number at tos to number 3rd on the stack
\ usage: 1 3 2 u+ \ results in 3 3 on the stack
: u+  ( a b c - a+c b )  \ under-plus: add a number to 3rd on stack
  rot + swap ;

\ intent: quickly nip multiple values from the stack
\ usage: 1 2 3 4 2 nips \ results in 1 4 on the stack
: nips  ( ... n1 count - )  \ perform multiple nips
  0 ?do nip loop ;

\ intent: reverse order of count items on stack
\ usage: 1 2 3  3 reverse \ results in 3 2 1 on the stack
: reverse  ( ... count - ... )  \ reverse order of count items on stack
  1 + 1 max 1 ?do  i 1- roll  loop ;

\ intent: for use implemented "wraparound" when incrementing and decrementing variables
\ usage: 15 4 7 wrap \ results in 7 on the stack
: wrap  ( n min max - n )  \ keep n within min and mix using wraparound
  1 +  over -  >r  swap over -  r> swap over 2* +  swap mod + ;

\ intent: restrict a value within a given range
\ usage: 15 0 4 clamp \ results in 4 on the stack
: clamp  ( n min max - n )  \ keep value in range using clamp
  >r max r> min ;

\ intent: not used anywhere (but could be useful)
\ usage: mything dup .itemlink lastitem @ LINK!
: link!  ( item item-link lastitem-link - )  \ simple statically-compiled backwards-linked-list function
  dup @ rot ! ! ;

\ intent: rotate the bits in the given value n #bits times to the left
\ usage: 64 2 << \ results in 256 on the stack
: <<  ( n #bits - n )  \ left bitwise shift
  " LSHIFT " EVALUATE ; IMMEDIATE

\ intent: rotate the bits in the given value n #bits times to the right
\ usage: 64 2 >> \ results in 16 on the stack
: >>  ( n #bits - n )  \ right bitwise shift
  " RSHIFT " EVALUATE ; IMMEDIATE

\ intent: print the hexadecimal string representation of a number
\ usage: 150 h. \ prints out $96
: h.  ( n - )  \ print hex string of number
  ." $" h. ;

\ intent: print two numbers in order passed to stack (instead of in order fetched from stack)
\ usage: 10 20 2. \ prints 10 20
: 2.  ( x y - )  \ print 2 ints
  swap . . ;

\ intent: quickly drop multiple items from the datastack
\ usage: depth drops \ will clear the stack
: drops  ( ... #drops - ... )  \ drop multiple items
  0 ?do drop loop ;

\ intent: dynamically define a new word in the current wordlist from a given string
\ usage: " something" $create \ something is now a valid word in the current wordlist
: $create ( addr c - )  \ create a word from a given string
  skip-cl  get-current (wid-create) last @ >create ! ;
