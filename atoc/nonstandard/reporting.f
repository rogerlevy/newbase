
variable reporting \ holds report string

\ intent: print a reverse-video string for development message reporting
\ usage: report" <string>"
: report"  \ compiling: ( "ccc<">" -- )  executing: ( -- )
  reporting @ not if  [char] " parse 2drop  exit  then
  postpone s"  s" inverse  cr  type  normal " evaluate ;  immediate

\ todo: ideally REPORT" can be used in compile or interpret mode...

\ intent: print a reverse-video string for development message reporting
\ usage: report( <string>)
: report(  ( -- )  \ report at compile time
  reporting @ not if  [char] ) parse 2drop  exit  then
  inverse cr [char] ) echo-until normal ; immediate

\ intent: display a windows OS alert message box
\ usage: <message> alert
: alert  ( adr c - )  \ show windows OS message box
  z$ 0 swap z" Alert" MB_OK MessageBox drop ;
