\ ==============================================================================
\ ForestLib > GameBlocks [2]
\ Vector tables [2]
\ ========================= copyright 2014 Roger Levy ==========================

\ {RL} we need examples - I put a task in the punchlist.

\ for version 3:
\  [ ] use an external buffer to allow descendent vtables to add new actions freely (no need for max actions)

0 value vt  \ current defining vtable (for ACTION not ::)

pstruct %vtable \ vtable data structure
  var fetcher  ( action -- ofs vtable )
  var rootvt \ {_BRIEF}
  var nextv \ {_BRIEF}
  var #actions \ {_BRIEF}
  init: actions \ {_BRIEF}

\ intent: {_INTENT}
\ usage: {_USAGE}
  : n/a abort" Parameter-returning action not implemented by vtable." ;

  create dse      \ default stack effects
    ' noop , ' drop , ' 2drop , ' 3drop , ' 4drop ,

  \ action struct:  offset , root-vtable , default-xt , current-vtable-fetcher , #ins , #outs ,

  \ fetcher: ( action -- offset vtable )

\ intent: {_INTENT}
\ usage: {_USAGE}
  : nextvofs>  ( - n )  \
    vt .nextv @ cells   vt .nextv ++ ;

\ intent: {_INTENT}
\ usage: {_USAGE}
  : rootvt>  ( - vtable )  \
    vt .rootvt @  ;

\ intent: {_INTENT}
\ usage: {_USAGE}
  : default-action  ( #ins #outs - xt )  \
    if  drop  ['] n/a  else  cells dse + @  then ;

\ intent: {_INTENT}
\ usage: {_USAGE}
  : action>slot  ( action - adr )  \
    dup  3 cells + @ execute  + ;

\ intent: define an action
\ usage: <input count> <output count> action <name>
  : action  ( #ins #outs - <name> )  \ nextv starts at 5 (skipping the vtable properties)
    >in @ exists if  drop  2drop exit  then >in !
    create
      nextvofs> ,  rootvt> ,  ( i o ) 2dup default-action ,  rootvt> .fetcher @ ,  swap , ,
    does>
      dup >r action>slot @ ?dup if
        r> drop  execute
      else
        r> cell+ cell+ @ execute  \ if hasn't been defined (0) in current vtable, execute the action's default xt.
      then ;

\ intent: set the current vtable
\ usage: <vtable> actions
  : actions: ( vtable - )  \ set vt
    to vt ;

\ intent: get the address of an action from the current vtable
\ usage: <action> vt[]
  : vt[] ( action - addr )  \  get action 
    @ vt + ;

\ intent: used to override a parent's method in a subclass
\ usage: :: <action>
  : ::  ( - <action> )  \ override operator
    & :noname swap vt[] ! ;

\ intent: set an action override
\ usage: <xt> >:: <action>
  : >::  ( xt - <action> )  \ to override
    & vt[] ! ;

\ intent: make an action do nothing
\ usage: don't <action>
  : don't  ( - <action> )  \ noop an action
    & >r  r@ 4 cells + 2@ default-action  r> vt[] ! ;

\ intent: get a copy of given vtable
\ usage: <vtable> vbeget
  : vbeget  ( vtable - new-vtable )  \ get copy of vtable
    with
    here dup to vt
      s@ %vtable sizeof copy,
      actions #actions @ cells copy, ;

\ intent: create a vtable
\ usage: [[ actorvt @ ]] 50 vtable,
  : vtable,  ( vtable-fetcher #maxmethods - )  \ create vtable
    here to vt  swap ,  vt ,  0 .actions cell/ ,  dup ,  0 ,  cells /allot ;

\ intent: create a named vtable
\ usage: [[ actorvt @ ]] 50 vtable myvtable
  : vtable  ( vtable-fetcher #maxmethods - <name> )  \  create named vtable
    create vtable, ;


\ intent: used to make different actions in a vtable have different vtable-fetchers
\ usage: <xt> <vtable> action-type
  : action-type  ( xt vtable - )  \
    .rootvt @ .fetcher ! ;

endp
