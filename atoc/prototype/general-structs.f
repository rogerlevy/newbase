\ ==============================================================================
\ ForestLib
\ General purpose structs
\ ========================= copyright 2014 Roger Levy ==========================

\ [defined] general_struct~ [if] \\ [then] plugin exposed

\ module ~general-structs

\ 4/24/13 created as structs.f
\ 6/5/13 pared down and saved as general_struct.f

\ intent: fetch value from S register
\ usage: s@
0 value s@ \ S register

\ intent: place a value into the S register
\ usage: <value> s!
: s!  ( - )  \ store value in s register
  to s@ ;

defer ifield  \ implicit field
  \ IFIELD is now defered which lets us use the same syntax to define fields for other s@ systems
  \ the universal feature remaining explicit target syntax ( e.g. >FIELDNAME )
  \
  \ with and s. are NOT defered.
  \  it's safe to do it with fields because it's compile time, but not anything in runtime code.
  \
  \ conceptually we need to regard "structs" as general purpose, stuff like arrays, vectors, and other "utilities"
  \  stuff where it makes most sense to "get in and get out".
  \  other kinds of structs, like entities, tasks, or other modal stuff where it's convenient
  \  to work with them as if they were global states, should have their own "set the current ____" words.

defer $create-field ( addr c )
defer +psize ( n )

\ intent: {_INTENT}
\ usage: {_USAGE}
: ds-$create-field  ( oldsize fieldsize adr c - oldsize fieldsize )  \ {_BRIEF}
  $create over , ;

\ intent: {_INTENT}
\ usage: {_USAGE}
: ds+psize  ( oldsize fieldsize - newsize )  \ {_BRIEF}
  + ;

\ intent: tell prototype we want to use old style data structures (which are currently being phased out)
\ usage: old-structs
: old-structs  ( - )  \ set up old structs
  ['] ds-$create-field is $create-field
  ['] ds+psize is +psize ; old-structs

\ intent: create a named field
\ usage: create-field <name>
: create-field  ( - <name> )  \ create named field
  bl parse $create-field ;

\ implicit target

\ intent: {_INTENT}
\ usage: {_USAGE}
: struct-ifield  ( ... size - <name> ... )  \ {_BRIEF}
  create-field does> @ s@ + ;

\ intent: tell prototype to use struct-ifield for implicit fields
\ usage: general-struct
: general-struct  ( - )  \ set up implicit field
  ['] struct-ifield is ifield ;  general-struct

\ intent: to be used for optimization of compiling words by inlining literals
\ usage: <literal> ?literal
: ?literal  ( literal - )  \ compile literal if in compile mode
  state @ if postpone literal then ;

\ explicit target

\ intent: {_INTENT}
\ usage: {_USAGE}
: create-efield  ( ... size - <name> ... )  \ {_BRIEF}
  " ." <$ bl parse $+ $> $CREATE-field immediate DOES> @ ?literal " + " evaluate ;

\ intent: {_INTENT}
\ usage: {_USAGE}
: efield  ( ... size - <name> ... )  \ {_BRIEF}
  >in @ >r  create-efield  r> >in ! ;

\ intent: define a field
\ usage: <size> field <name>
: field  ( ... size - <name> ... )  \ define field
  efield  ifield  +psize ;

\ intent: define a single-cell sized field
\ usage: var <name>
: var  ( - <name> )  \ define field
  cell field ;

\ intent: define a double-cell sized field
\ usage: dvar <name>
: dvar  ( - <name> )  \ define field
  2 cells field ;

\ object stack - needed to play nice with locals, circular for stability, and supports multitasking
\           by allowing relocation of the stack (just say TO OSTACK)
\           note: both S and OSP need to be saved/restored on task switches
32 cells constant /ostack
create ostack0  /ostack /allot
ostack0 value ostack
0 value osp \ offset into ostack

\ intent: place value on object stack
\ usage: <value> >o
: >o  ( n - )  \ to object stack
  osp ostack + !  osp cell+ [ /ostack 1 - ]# and to osp ;

\ intent: take value from object stack and place on data-stack
\ usage: o>
: o>  ( - n ) \ from object stack
  osp cell- [ /ostack 1 - ]# and to osp  osp ostack + @ ;

\ intent: save S register on object stack then set S register to address on tos
\ usage: <addr> save-s!
macro: save-s!  ( addr - )  s@ >o  to s@ ;  \ save and set S register

\ intent: set S register to address on object stack 
\ usage: restore-s
macro: restore-s  ( - )  o> to s@ ;  \ restore S register to previously saved value

\ NOTE: (with) IS NOT PORTABLE because of CALL word.
\ needs a portable reimplementation.
\ intent: factor word for with
\ usage: {DO NOT USE DIRECTLY}
: (with)  ( adr - )  \ compiled by WITH, not for direct use
  s@ over = if  drop  exit  then  save-s!  r> call  restore-s ;

\ intent: store the address of given object in S register
\ usage: <object> with
: with  ( addr - )  \ focus current object for remainder of definition using the S register
  state @ if postpone (with) else to s@ then ; immediate

\ intent: this is now deprecated
\ usage: {DO NOT USE}
: ->  ( adr - <word> )  \ target given object in following word. i'm trying to phase this out.
  drop -30 throw ;

\ since i intend to implement objects that set the current whatever by invoking their name,
\ i'm providing for getting the address of such objects ...

\ intent: take away the does> portion of a word definition to obtain the word's address - used outside of definitions
\ usage: & <word>
: &  ( - <word> adr )  \ get address of word; stripping does>
  ' >body ;

\ intent: take away the does> portion of a word definition to obtain the word's address - used inside of definitions
\ usage: [&] <word>
: [&]  ( - <word> adr )  \ get address of word; stripping does>
  ' >body postpone literal ; immediate

\ intent: force the size of structure
\ usage: {pending removal - not to be used}
\ : force-size  ( oldsize newsize - newsize )  \ force size of structure
\  nip ;
