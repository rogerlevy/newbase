1 strands here 0 ,
create %class  /class , , ' noop , ' pstruct-fields , , 0 , %class , including string,

1 strands here 0 ,
create %object  0 , , ' noop , ' pstruct-fields , , 0 , %class , including string,

\ support for nested class definitions.  note that classes are not meant to be private, just "members".
create pdefs 16 cells allot  \ pdef stack
variable >pdef               \ index into pdef stack

\ intent: define the code which will access the field
\ usage: addressing: <code> ;
: addressing:  ( - <code> ; ) ( offset -- address ) \ code should tell how to get at the field
  " [[ [[ create-field does> @ " <$
  [char] ; parse $+
  " ]] is ifield ]] fields" $+ $> evaluate ;

\ intent: push prototype definition
\ usage: push-pdef
: push-pdef  ( - )  \ push pdef
  pdef >pdef @ 15 and cells pdefs + !  >pdef ++ ;

\ intent: pop prototype definition
\ usage: pop-pdef
: pop-pdef  ( - )  \ pop pdef
  >pdef --  >pdef @ 15 and cells pdefs + @ to pdef ;

\ intent: set the onbeget field of the current prototype to the given xt
\ usage: <xt> begetting
: begetting  ( xt - )  \ set prototype onbeget
  pdef .onbeget ! ;

\ intent: fetches prototype of given class
\ usage: %<classname> >prototype
macro: >prototype  .proto @ ;  ( class - prototype )  \ fetch class prototype

\ intent: {_INTENT}
\ usage: {_USAGE}
: pdef-$create-field  ( adr c - )  \ {_BRIEF}
  $create pdef .size @ , ;

\ intent: {_INTENT}
\ usage: {_USAGE}
: pdef+psize  ( n - )  \ {_BRIEF}
  pdef .size +! ;

\ intent: enable classes to be created instead of POD structs
\ usage: new-structs
: new-structs  ( - )  \ enable classes
  ['] pdef-$create-field is $create-field
  ['] pdef+psize is +psize ; new-structs

\ intent: prototype here (get the address following current class definition)
\ usage: phere
: phere  ( - adr )  \ prototype here
  prototype pdef .size @ + ;

\ intent: create named cell-sized field
\ usage: <value> var: <name>
: var:   ( value -- <name> )  \ int field
  phere ! var ;

\ intent: create named float field
\ usage: <f:value> fvar: <name>
: fvar:  ( f: value - <name> )  \ float field
  phere f! dvar ;

\ intent: create named single-precision float field
\ usage: <f:value> sfvar: <name>
: sfvar:  ( f: value - <name> )  \ 32-bit float field
  phere sf! var ;

\ intent: create a named field of unknown type and unknown size
\ usage: init: <name>
: init:  ( - <name> )  \ init named field
  0 field ;

\ intent: used to comma a value into the class structure
\ usage: <value> ^
: ^  ( n - )  \ hoist value
  phere !  cell +psize ;

\ intent: create named string field
\ usage: <string> string: <name>
: string:  ( adr c - <name> )  \ named string field
  phere place  128 field ;

\ intent: null operation function pointer style named field creation
\ usage: noop: <name>
: noop:  ( - <name> )  \ create a noop field
  ['] noop var: ;

\ intent: embed another object as a field of a class
\ usage: %<classname> inline <name>
: inline  ( ... class - <name> ... )  \ embed another object
  with  proto @ phere size @ move  s@ sizeof field ;

\ intent: subclass another class ( a form of inheritance )
\ usage: %<parent-classname> ~> %<subclass-classname>
: ~>  ( class - <name> )  \ subclass
  push-buf  push-pdef  \ new-structs
  derive  1 strands locals| wl parent |
  create  here  dup s!  to pdef
  parent dup .classclass @ sizeof copy,
  parent ancestor !
  wl pwl !
  including ppath place
  onbeget @ execute  ptype @ execute
  pdef +innards ;

\ intent: finish up the prototype and copy the data into a unique buffer
\ usage: endp
: endp  ( - )  \ end class
  pdef with  pdef -innards
  pop-pdef
  here proto !  prototype size @ copy,
  pop-buf ;

\ intent: allow mixin extension functionality for existing class
\ usage: %<classname> reopen
: reopen  ( class - )  \ class mixin
  push-buf  pdef >o  dup +innards  dup s!  to pdef
  prototype /buf erase  proto @ prototype size @ cmove  ptype @ execute ;

\ intent: create named class (start a structure definition)
\ usage: pstruct %<classname>
: pstruct  ( - <name> )  \ create named class
  %object ~> ;
