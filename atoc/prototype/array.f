\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ RM: I am nodocing this file for now.
\ I need to spend some time with these words and play around with them and see
\ how and what they are doing. Also, this file REALLY needs an overhaul as far
\ as organization goes. We can move on with things, and I can tackle this stuff
\ later. I will do the 'playing' on my own time.
\ ==============================================================================
\ ForestLib
\ General purpose Arrays
\ ========================= copyright 2014 Roger Levy ==========================

\ generic array data structure and operations are also shared with a generic stack data structure
\ many words are subject to be removed and/or renamed so use with care.

pstruct %array  \ generic array class
  var #items    
  var maxitems
  var itemsize
  var 'item
  var stack?
  var (items)
  \ for 2D support
  var cols
  var rows
endp

0 value arr  \ array being iterated on

\ intent: get the size of array structure
\ usage: /array
: /array  ( - n)  \ size of array
  %array sizeof ;

\ intent: get access to items of an array
\ usage: items
: items  ( - adr )  \ get address of first item
  " (items) @ " evaluate ; immediate

\ intent: get access to items of an array
\ usage: <array> >items
: >items  ( array - adr )  \ get address of first item
  " .(items) @ " evaluate ; immediate

\ intent: {INTENT}
\ usage: {USAGE}
: (#items)  ( {ins} - {outs} )  \ {BRIEF}
  dup .stack? @ if .#items @ else .maxitems @ then ;

\ intent: {INTENT}
\ usage: {USAGE}
subst (#items) length  ( {ins} - {outs} )  \ {BRIEF}

\ intent: {INTENT}
\ usage: {USAGE}
: each  ( XT array -- ) ( ... addr -- ... )  \ itterate on array
  dup .#items @ 0= if  2drop  exit  then
  s@ >r
  arr >r  to arr
  arr >items  arr length  arr .itemsize @ * bounds do
    i over >r  swap execute  r>
  arr .itemsize @ +loop
  drop
  r> to arr
  r> s! ;

\ intent: {INTENT}
\ usage: {USAGE}
: <EACH  ( {ins} - {outs} )  \ {BRIEF}
  dup .#items @ 0= if  2drop  EXIT  then
  s@ >r
  arr >r  to arr
  arr >items  arr (#items) 1 -  arr .itemsize @ * BOUNDS swap DO
    i over >r  swap EXECUTE  r>
  arr .itemsize @ negate +LOOP
  DROP
  r> to arr
  r> s! ;

\ intent: {INTENT}
\ usage: {USAGE}
: (item)  ( array - )  \ {BRIEF}
  with itemsize @ * items + ;

\ intent: {INTENT}
\ usage: {USAGE}
: cell[]  ( n array - addr )  \ {BRIEF}
  >items >r cells r> + ;

\ intent: {INTENT}
\ usage: {USAGE}
: half[]  ( {ins} - {outs} )  \ {BRIEF}
  >items >r 1 << r> + ;

\ intent: {INTENT}
\ usage: {USAGE}
: char[]  ( {ins} - {outs} )  \ {BRIEF}
  >items + ;

\ intent: {INTENT}
\ usage: {USAGE}
: []  ( n array - addr )  \ {BRIEF}
  dup .'item @ execute ;

\ intent: {INTENT}
\ usage: {USAGE}
: first[]  ( array - addr )  \ {BRIEF}
  0 swap [] ;

\ intent: {INTENT}
\ usage: {USAGE}
: last[]  ( array - addr )  \ {BRIEF}
  dup length 1 - swap [] ;

\ intent: {INTENT}
\ usage: {USAGE}
: itemsbounds  ( array - addr addr )  \ {BRIEF}
  dup first[] swap with itemsize @ #items @ * bounds ;

\ intent: {INTENT}
\ usage: {USAGE}
: push  ( value array - )  \ {BRIEF}
  locals| arr |
  arr .#items @ arr cell[] ! arr .#items ++ ;

\ intent: {INTENT}
\ usage: {USAGE}
: pop  ( array - value )  \ {BRIEF}
  locals| arr |
  arr .#items -- arr .#items @ arr cell[] @ ;

\ intent: {INTENT}
\ usage: {USAGE}
: top@  ( array - value )  \ {BRIEF}
  locals| arr |
  arr .#items @ 1 - arr [] @ ;

\ intent: {INTENT}
\ usage: {USAGE}
: vacate  ( array - )  \ {BRIEF}
  .#items off ;

\ {brief}
create []'s  ' char[] , ' half[] , ' (item) , ' cell[] , ' (item) ,

\ intent: {INTENT}
\ usage: {USAGE}
: *stack  ( max-items itemsize - )  \ {BRIEF}
  2dup itemsize ! maxitems !
  itemsize @ 5 min 1 - cells []'s + @ 'item !
  stack? on
  here (items) !
  * /allot
;

\ intent: {INTENT}
\ usage: {USAGE}
: (stack)  ( max-items itemsize - )  \ {BRIEF}
  here with /array /allot  *stack
;

\ intent: {INTENT}
\ usage: {USAGE}
: init-stack  ( max-items array - )  \ {BRIEF}
  with  cell *stack 0 #items ! ;

\ intent: {INTENT}
\ usage: {USAGE}
: stack,  ( max-items - )  \ {BRIEF}
  cell (stack) ;

\ intent: {INTENT}
\ usage: {USAGE}
: array,  ( #items item-size - )    \ {BRIEF}
  here with  over swap (stack) #items !  stack? off ;

\ intent: {INTENT}
\ usage: {USAGE}
: init-array  ( #items item-size array - )  \ {BRIEF}
  with itemsize !  dup #items ! maxitems !
  here (items) !
  itemsize @ 5 min 1 - cells []'s + @ 'item ! ;

\ compile array header into dictionary - doesn't allocate the space for the actual items

\ intent: {INTENT}
\ usage: {USAGE}
: arrayheader,  ( #items item-size - )  \ {BRIEF}
  here  /array /allot  init-array ;

\ step through without EACH - does not preserve s@
create arrs  32 cells cell+ /allot

\ intent: {INTENT}
\ usage: {USAGE}
: arrs>  ( - addr )  \ {BRIEF}
  arrs cell+ arrs @ 31 cells and + ;

\ intent: {INTENT}
\ usage: {USAGE}
: /each  ( s@=array - s@=item )  \ {BRIEF}
  s@  arr arrs> !  cell arrs +!  dup to arr  >items s! ;

\ intent: {INTENT}
\ usage: {USAGE}
: each/  ( - )  \ {BRIEF}
  -cell arrs +!  arrs> @ to arr  ;

\ intent: {INTENT}
\ usage: {USAGE}
: +item  ( {ins} - {outs} )  \ {BRIEF}
  arr .itemsize @ s@ + s! ;

\ intent: {INTENT}
\ usage: {USAGE}
: check  ( ... xt array - ... flag ) ( ... -- break? ... )  \ {BRIEF}
  with  locals| xt |
  /each  arr length 0 do  xt execute  ?dup if  unloop  exit  then  loop  false ;

\ : ?NXT  nxt  s@ arr >items - arr .itemsize @ /  arr .#items @ >= ;

\ itterate on contents of stack
0 value xt

\ intent: {INTENT}
\ usage: {USAGE}
: those  ( xt stack - )  \ {BRIEF}
  xt >r  swap to xt
  [[ @ xt execute ]] swap each
  r> to xt ;

\ intent: {INTENT}
\ usage: {USAGE}
: <those  ( xt stack - )  \ {BRIEF}
  xt >r  swap to xt
  [[ @ xt execute ]] swap <each
  r> to xt ;

\ intent: {INTENT}
\ usage: {USAGE}
: sort  ( xt array - ) ( item1 item2 - flag )  \ {BRIEF}
  with  (items) @ #items @ rot cellsort ;
\ if flag returned is true, then a is less than b.

\ intent: get a random item and index from an array
\ usage: <array> rnditem
: rnditem  ( array - item item# )  \ get random item and index
  dup length rnd dup >r swap [] r> ;

\ intent: get a random item from an array
\ usage: <array> rnd@
: rnd@  ( array - value )  \ get random value
  rnditem drop @ ;

\ intent: get the address of a random array item
\ usage: <array> rnd[]
: rnd[]  ( array - item )  \ get random item
  rnditem drop ;

\ in interpret mode these will behave like this.
\ in compile mode they will have an IMMEDIATE behavior that returns an array stored
\ in a region.

\ intent: {INTENT}
\ usage: {USAGE}
: [array  ( width - width array )  \ {BRIEF}
  here swap  0 over arrayheader, here ;

\ intent: {INTENT}
\ usage: {USAGE}
: array]  ( {ins} - {outs} )  \ {BRIEF}
  rot with  here swap - swap /  dup #items !  maxitems ! ;

\ intent: {INTENT}
\ usage: {USAGE}
: items!  ( addr - )  \ {BRIEF}
  .(items) ! ;

\ intent: {INTENT}
\ usage: {USAGE}
: array!  ( mem #items item-size array - )  \ {BRIEF}
  with  s@ init-array  s@ items! ;

\ intent: {INTENT}
\ usage: {USAGE}
: stack!  ( mem #items array - )  \ {BRIEF}
  with  s@ init-array  s@ items! ;

\ intent: {INTENT}
\ usage: {USAGE}
: []wrap  ( n array - item )  \ {BRIEF}
  with  #items @ mod  s@ 'item @ execute ;

\ intent: {INTENT}
\ usage: {USAGE}
: []clamp  ( n array - item )  \ {BRIEF}
  with  0 #items @ 1 - clamp  s@ 'item @ execute ;

\ intent: {INTENT}
\ usage: {USAGE}
subst []wrap wrap[]

\ intent: {INTENT}
\ usage: {USAGE}
subst []clamp clamp[]

\ this is a pretty bad shuffle algorithm.

\ intent: shuffle array
\ usage: <array> shuffle
: shuffle  ( {ins} - {outs} )  \ {BRIEF}
  3 0 do  [[ 2drop 2 rnd ]] over sort  loop  drop ;
