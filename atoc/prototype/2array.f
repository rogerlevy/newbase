\ ==============================================================================
\ ForestLib > GameBlocks
\ 2D arrays
\ ========================= copyright 2014 Roger Levy ==========================

\ IDEA:
\  roweach   - ask {RL} regarding what this means

\ intent: comma an array into the dictionary
\ usage: <columns> <rows> <item size> 2array,
: 2array,  ( w h itemsize - )  \ create array
  >r  2dup *  r>  here with  arrayheader,  swap cols 2! ;

\ intent: get address for given array coordinate
\ usage: <column> <row> <2array> [][]
: [][]  ( col row 2array - adr )  \ index 2array
  dup >r  .cols @ * +  r> [] ;

\ intent: copy arrays (src and dest item sizes must be the same, and CELL aligned)
\ usage: <column> <row> <source> <column> <row> <dest> <columns> <rows> 2move
: 2move  ( col row  src-array  col row dest-array  #cols #rows - )  \ copy from one array to another
  third .itemsize @ cell/ locals| item rows cols dest drow dcol src srow scol |
  rows 0 do
    scol srow i + src [][]
    dcol drow i + dest [][]  item cols * imove
  loop ;
  \ todo: clipping

\ intent: fill a section of array with data (for 2array's with cell-sized items only)
\ usage: <value> <column> <row> <columns> <rows> <2array> 2fill
: 2fill  ( value col row #cols #rows dest-array - )  \ fill array
  locals| dest rows cols row col val |
  rows 0 do
    col row i + dest [][] cols val ifill
  loop ;
  \ todo: clipping

\ intent: translate array into data for 2fill
\ usage: <2array> 2entire
: 2entire  ( 2array - 0 0 #cols #rows 2array )  \ translate array into data for 2fill
  with 0 dup cols 2@ s@ ;

\ intent: fill entire array with zeroes
\ usage: <2array> 2clear
: 2clear ( 2array - )  \ zero-out array
  0 swap 2entire 2fill ;

\ intent: print out array
\ usage: <2array> .cells
: .cells  ( 2array - )  \ print array cells
  with
  rows @ 0 do
    cr
    cols @ 0 do
      i j s@ [][] @ 9 h.0 space
    loop
  loop ;
