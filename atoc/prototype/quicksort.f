\ ==============================================================================
\ ForestLib
\ Quicksort
\ ========================= copyright 2014 Roger Levy ==========================

\ credit for forth code: wil baden
\ source: http://en.literateprograms.org/Quicksort_%28Forth%29

\ can support comparison of indirect values (such as the z coordinate of a entity) by reassigning ( sort@ ) to
\   a bit of code that will fetch the field of the object in the list.

\   usage:  :noname  ( addr -- n ) <code>  ; is sort@
\           <cell array> sort

\ note that sorting an already sorted array does NOT guarantee all item positions to be the same as before.

package quicksorting

defer comparison ( a b - flag )  ' < is comparison  \ if flag returned is true, then a is less than b.

private

\ intent: get the midpoint between left and right points
\ usage: <left> <right> mid
: mid  ( l r - mid )  \ get midpoint
  over - 2/ -cell and + ;

\ intent: indirectly swap values via their addresses
\ usage: <addr1> <addr2> exchange
: exchange  ( addr1 addr2 - )  \ indirectly swap values
  dup @ >r over @ swap ! r> swap ! ;

\ intent: factor-word for (cellsort)
\ usage: {not used externally}
: part  ( l r - l r r2 l2 )  \ low-level partitioning algorithm
  2dup mid @ ( sort@ ) >r ( r: ppivot )
  2dup begin
    swap begin dup @ ( sort@ )  r@ comparison while cell+ repeat
    swap begin r@ over @ ( sort@ ) comparison while cell- repeat
    2dup <= if 2dup exchange >r cell+ r> cell- then
  2dup > until  r> drop ;

\ intent: factor word for cellsort
\ usage: {not used externally}
: (cellsort)  ( l r - )  \ high-level partitioning algorithm
  part  swap rot
  2dup < if recurse else 2drop then
  2dup < if recurse else 2drop then ;

public

\ intent: sort a contiguous array
\ usage: <array address> <array length> <comparator xt> cellsort
: cellsort ( addr count comparison-xt - )
   is comparison
   dup 1 > if 1 - cells over + (cellsort) else 2drop then ;

end-package
