\ ==============================================================================
\ ForestLib
\ Prototype-based OOP
\ ========================= copyright 2014 Roger Levy ==========================

include general-structs \ bring in general data structure support

16384 constant /buf                     \ size of a prototype definition buffer
create prototype-bufs /buf 8 * /allot   \ prototype definition buffer stack
variable >buf                           \ index into prototype buffer stack

\ intent: increment buffer stack index
\ usage: push-buf
: push-buf   ( - ) \ push new buffer
  >buf ++ ;

\ intent: decrement buffer stack index
\ usage: pop-buf
: pop-buf  ( - ) \ pop current buffer
  >buf -- ;

\ intent: gets the address of the current prototype being defined
\ usage: prototype
: prototype  ( - adr )  \ current prototype address
  >buf @ 7 and /buf * prototype-bufs + ;

0
  var size
  var proto          \ link to class
  var onbeget        \ XT ( class - class ) used to inherit any external data or do other tasks on ~>
  var ptype          \ XT - used to alter field behavior and do any state setting on ~> and REOPEN
  var pwl            \ innards wordlist - it's a factoring tool.  descendents "inherit" it.
  var ancestor       \ parent class
  var classclass     \ class of this class
  256 field ppath    \ path to file the prototype was defined in
constant /class

\ remnants from old code - A register is not used now - leaving as reminder.
\ [a] +order
\ p revious

\ intent: {_INTENT}
\ usage: {_USAGE}
: derive  ( prototype - prototype )  \ copies given prototype contents to the prototype buffer
  prototype /buf erase
  dup .size 2@ swap   prototype swap move \ copy prototype to buffer
  ;

\ -------------------------------------------------------------------------------------------
\ Instantiation
defer getmem

\ intent: copy default values of a class into the given instance memory -- the instance must NOT have been created by instance, and friends.
\ usage: %<classname> <instance address> initialize
: initialize  ( class addr - )  \ copy default values into instance
  >r 2@ r> rot cell/ imove ;

\ intent: used by instantiate to create objects in dictionary
\ usage: dict
: dict  ( - )  \ create object in dictionary
  [[ here swap allot ]] is getmem ;

\ intent: used by instantiate to create objects on the heap
\ usage: {not used - don't use this}
: sysheap  ( - )  \ create object on heap
  [[ allocate throw ]] is getmem ;

\ intent: allocate the memory for a class instance (think of this sort of as a 'new' operator)
\ usage: %<classname> instantiate
: instantiate  ( class - addr )  \ allocate instance memory
  with  size @ getmem >r  size 2@ r@ rot cell/ imove  r> ;

\ intent: create an instance of a class - the address of the instance is left on tos
\ usage: %<classname> instance
: instance  ( class - instance )  \ create an instance of a class
  dict instantiate ;

\ intent: create an instance of a class
\ usage: %<classname> instance,
: instance,  ( class - )  \ create an instance of a class
  instance drop ;

\ intent: copy one class to another
\ usage: <source> <dest> %<classname> clone
: clone   ( src dest class - )  \ copy one class to another
  .size @ cell/ imove ;

\ intent: create a named instance of a class
\ usage: %<classname> object <name>
: object   ( class - <name> )  \ create an instance of a class
  create instance, ;

0 value pdef    \ current class definition
0 value n       \ wordlist counter

\ intent: factor word for +innards
\ usage: {not to be used externally}
: (innards)  ( class - wordlists... )
  ?dup -exit   1 +to n  dup .pwl @    swap .ancestor @ recurse ;

\ intent: enter class innards
\ usage: %<classname> +innards
: +innards  ( class - )  \ enter class innards
  0 to n  (innards)  drop  n 1 - 0 ?do +order loop ;

\ intent: exit class innards
\ usage: %<classname> -innards
: -innards  ( class - )  \ exit class innards
  ?dup -exit   dup .pwl @ -order .ancestor @ recurse ;

\ intent: {_INTENT} {RM: Okay Roger, I need you to explain 'in' once again. I thought I knew what it did, but I was wrong.}
\ usage: in <name>
: in  ( - <name> )  \ {_BRIEF}
  & +innards ;

\ intent: set ptype to given xt and execute ptype
\ usage: <xt> fields
: fields  ( xt - )  \ set and execute ptype 
  dup pdef .ptype ! execute ;

variable (current)  \ current wordlist id ( used by [i <code> i] enclosure system )

\ intent: begin implemention of a private context for a chunk of code
\ usage: [i <code> i]
: [i  ( - )  \ start internal block  ( for factor words )
  (current) @ >o    get-current (current) !   pdef .pwl @ set-current ;

\ intent: end implementation of a private context for a chunk of code
\ usage: [i <code> i]
: i]  ( - )  \ end internal block  ( for factor words )
  (current) @ set-current   o> (current) ! ;

\ this is a really dangerous word and should only be used for debugging and experimentation in the IDE
\ or in very special circumstances where you know you haven't d anything that will use the
\ feature(s) you're about to add.

\ intent: add additional functionality to a class after the definition of the class
\ usage: %<classname> reopen <code> endp
: reopen  ( class - class size )  \ allow for a class 'mixin'
   dup +innards
   dup s!   prototype /buf erase   proto @ prototype size @ cmove   size @   ptype @ execute ;

\ intent: get the size of a class
\ usage: %<classname> sizeof
: sizeof  ( class - sizeof )  \ size of class
  .size @ ;

\ intent: {_INTENT}
\ usage: {_USAGE}
: pstruct-fields  ( - )
  general-struct   prototype s! ;

include prototype-b \ bring in the new prototype implementation
include quicksort \ bring in quicksort utility
include array \ bring in array
