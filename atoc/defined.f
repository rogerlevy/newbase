
\ intent: to check for the existance of the following name
\ usage: #defined <named-constant>
: #defined  ( - flag )  \ checks for the following name to exist in the dictionary
  bl word find 0<> nip ;

\ intent: to check for the absence of the following name
\ usage: #undefined <named-constant>
: #undefined  ( - flag )  \ checks that the following name does not exist in the dictionary
  #defined not ;

\ polyfills to have compatibility with SwiftForth's word checking words
#undefined [defined] [if]  subst #defined [defined]  immediate  [then]
#undefined [undefined] [if]  subst #undefined [undefined]  immediate  [then]
