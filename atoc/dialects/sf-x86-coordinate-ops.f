\ {nodoc} tell the documentation progress monitor tool to ignore this file
ICODE 2+  ( x y x y - x y )  \ documented in: misc.f
  0 [ebp] eax mov  \ get d2-lo
  eax 8 [ebp] add  \ add d1-lo
  4 [ebp] ebx add  \ add d1-hi
  8 # ebp add      \ and clean up stack
  ret end-code

ICODE 2-  ( x y x y - x y )  \ documented in: misc.f
  0 [ebp] eax mov
  ebx 4 [ebp] sub
  eax 8 [ebp] sub
  4 [ebp] ebx mov
  8 # ebp add
  ret end-code

ICODE 3+  ( x y z x y z - x y z )  \ documented in: misc.f
  0 [ebp] eax mov
  4 [ebp] ecx mov
  ebx 8 [ebp] add
  eax 12 [ebp] add
  ecx 16 [ebp] add
  8 [ebp] ebx mov
  12 # ebp add
  ret end-code

ICODE 3-  ( x y z x y z - x y z )  \ documented in: misc.f
  0 [ebp] eax mov
  4 [ebp] ecx mov
  ebx 8 [ebp] sub
  eax 12 [ebp] sub
  ecx 16 [ebp] sub
  8 [ebp] ebx mov
  12 # ebp add
  ret end-code

ICODE xswap ( x1 y1 x2 y2 - x2 y1 x1 y2 )  \ documented in: misc.f
  0 [ebp] ecx mov
  8 [ebp] eax mov
  ecx 8 [ebp] mov
  eax 0 [ebp] mov
  ret end-code

ICODE yswap ( x1 y1 x2 y2 - x1 y2 x2 y1 )  \ documented in: misc.f
  ebx ecx mov
  4 [ebp] ebx mov
  ecx 4 [ebp] mov
  ret end-code

ICODE area  ( x y w h - x1 y1 x2 y2 )  \ documented in: misc.f
  8 [ebp] eax mov
  4 [ebp] ebx add
  eax 0 [ebp] add
  ret end-code
