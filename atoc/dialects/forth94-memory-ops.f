\ {nodoc} tell the documentation progress monitor tool to ignore this file
subst 2@ d@  ( addr - d )  \ documented in: misc.f
subst 2! d!  ( d addr - )  \ documented in: misc.f
subst 2variable dvariable ( - <name> )  \ documented in: misc.f

: 2!  ( x y addr - )  \ documented in: misc.f
  dup >r  cell+ !  r> ! ;

: 2@  ( addr - x y )  \ documented in: misc.f
  dup @  swap cell+ @ ;

: 2+!  ( x y addr - )  \ documented in: misc.f
  dup >r 2@ 2+ r> 2! ;

: d+!  ( d addr - )  \ documented in: misc.f
  dup >r d@ d+ r> d! ;

: 3!  ( x y z addr - )  \ documented in: misc.f
  dup >r 2 cells + ! r@ cell+ ! r> ! ;

: 3+!  ( x y z addr - )  \ documented in: misc.f
  dup >r 2 cells + +! r@ cell+ +! r> +! ;

: 3@  ( addr - x y z )  \ documented in: misc.f
  @+ swap @+ swap @ ;

