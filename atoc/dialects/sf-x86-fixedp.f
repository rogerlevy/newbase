
\ intent: multiply two fixed-point values
\ usage: <n1.> <n2.> fixed-mul
icode fixed-mul  ( n. n. - n. )  \ fixed-point multiply
  ebx eax mov
  0 [ebp] imul
  4 # ebp add
  dx ax mov
  eax 16 # ror
  eax ebx mov
  ret end-code

\ intent: integer to fixed-point conversion
\ usage: <int> s>p
icode s>p  ( n - n. )  \ convert int to fixed
  EBX 16 # SHL
  ret end-code

\ intent: fixed-point to integer conversion
\ usage: <n.> p>s
icode p>s  ( n. - n )  \ convert fixed to int
  EBX 16 # SAR
  ret end-code

\ intent: integer 2D vector to fixed-point 2D vector conversion
\ usage: <v1> 2s>p
icode 2s>p  ( x y - x. y. )  \ convert 2 ints to fixed
  ebx 16 # shl
  0 [ebp] 16 # shl
  ret end-code

\ intent: fixed-point 2D vector to integer 2D vector conversion
\ usage: <v1.> 2p>s
icode 2p>s  ( x. y. - x y )  \ convert 2 fixed to int
  ebx 16 # sar
  0 [ebp] 16 # sar
  ret end-code

\ intent: integer 3D vector to fixed-point 3D vector conversion
\ usage: <v1> 3s>p
icode 3s>p  ( x y z - x. y. z. )  \ convert 3 ints to fixed
  ebx 16 # shl
  0 [ebp] 16 # shl
  4 [ebp] 16 # shl
  ret end-code

\ intent: fixed-point 3D vector to integer 3D vector conversion
\ usage: <v1.> 3p>s
icode 3p>s  ( x. y. z. - x y z )  \ convert 3 fixed to int
  ebx 16 # sar
  0 [ebp] 16 # sar
  4 [ebp] 16 # sar
  ret end-code

\ intent: round fixed-point value down to nearest integer (result is fixed-point value)
\ usage: <n.> floor
icode pfloor  ( n. - n. )  \ round down to integer
  $ffff0000 # ebx and  ret end-code
