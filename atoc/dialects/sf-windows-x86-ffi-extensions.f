\ {nodoc} tell the documentation progress monitor tool to ignore this file

\ -------------------------------------------------------------------------------------------------
\ FFI extension, included to support OpenGL extensions

[defined] lib-interface [if]
  package lib-interface
[else]
  package windows-interface
[then]

\ intent: {INTENT}
\ usage: {USAGE}
: NOPROC  ( pfa - )  \ {BRIEF}
  CODE> >NAME BAD-PROC ;

   Variable RETURNS-DATA  Variable C/PASCAL

\ intent: {INTENT}
\ usage: {USAGE}
   : 0PARMS
     [+ASM]
       4 # EBP SUB
       EBX 0 [EBP] MOV
       EBP PUSH
       ESI PUSH
       EDI PUSH
     [-ASM] ;

\ intent: {INTENT}
\ usage: {USAGE}
   : 1PARMS
     [+ASM]
       EBP PUSH
       ESI PUSH
       EDI PUSH
       EBX PUSH
     [-ASM] ;

\ intent: {INTENT}
\ usage: {USAGE}
   : NPARMS ( n - )
     [+ASM]
       1- CELLS ( n)
       DUP ( n) [EBP] EDX LEA
       EDX PUSH
       ESI PUSH
       EDI PUSH
       EBX PUSH
     [-ASM]
     DUP ( n) 0 DO
       I [+ASM] [EBP] PUSH  [-ASM]
     CELL +LOOP
     [+ASM]
       ( n) # EBP ADD
     [-ASM] ;

\ intent: {INTENT}
\ usage: {USAGE}
   : PROLOG ( n - )
     DUP 0=  IF  DROP 0PARMS  EXIT THEN
     DUP 1 = IF  DROP 1PARMS  EXIT THEN
     NPARMS ;

\ intent: {INTENT}
\ usage: {USAGE}
   : EPILOG
     C/PASCAL @ IF
       [+ASM]
      C/PASCAL @ CELLS # ESP ADD
       [-ASM]
     THEN
     [+ASM]
       EDI POP
       ESI POP
       EBP POP
     [-ASM]
     RETURNS-DATA @ IF
       [+ASM] EAX EBX MOV [-ASM]
     ELSE
       [+ASM] POP(EBX) [-ASM]
     THEN  0 RETURNS-DATA !
     [+ASM]
       RET
     [-ASM] ;

\ intent: {INTENT}
\ usage: {USAGE}
   : WPCALL ( n - )
     [+ASM]
       HERE 13 - >R
       ECX POP
       0 [ECX] ECX MOV
       PROLOG
      ECX ECX OR  0= NOT IF
       ECX CALL  EPILOG  THEN
       R> CODE> # EBX MOV
       ['] NOPROC >CODE JMP
     [-ASM] ;

PUBLIC

\ intent: {INTENT}
\ usage: {USAGE}
  : void  returns-data off ;

\ intent: {INTENT}
\ usage: {USAGE}
  : (proc:) ( n proc - <name> )
    header
    here 13 + dup >r ,call          \ forward call to code compiled by wpcall
    ( proc ) ,                  \ compile address of proc
    0 ,                       \ ( void dll pointer )
    ( n ) wpcall                 \ compile in-line code to make the actual proc call
    r> code> ['] .proc decode, ;      \ make a way to decode it

\ intent: {INTENT}
\ usage: {USAGE}
  : proc:
    c/pascal off  (proc:)  returns-data on  -? ;

\ intent: {INTENT}
\ usage: {USAGE}
  : cproc:
    c/pascal off  (proc:)  returns-data on  -? ;

END-PACKAGE
