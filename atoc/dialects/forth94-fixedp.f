
\ intent: multiply two fixed-point values
\ usage: <n1.> <n2.> fixed-mul
: fixed-mul  ( n. n. - n. )  \ fixed-point multiply
  m* 16 >> swap 16 << or ;

\ intent: integer to fixed-point conversion
\ usage: <int> s>p
macro: s>p  ( n - n. )  \ convert int to fixed
  16 << ;

\ intent: fixed-point to integer conversion
\ usage: <n.> p>s
macro: p>s  ( n. - n )  \ convert fixed to int
  65536 / ;

\ intent: integer 2D vector to fixed-point 2D vector conversion
\ usage: <v1> 2s>p
: 2s>p  ( x y - x. y. )  \ convert 2 ints to fixed
  s>p swap s>p swap ;

\ intent: fixed-point 2D vector to integer 2D vector conversion
\ usage: <v1.> 2p>s
: 2p>s  ( x. y. - x y )  \ convert 2 fixed to int
  p>s swap p>s swap ;

\ intent: integer 3D vector to fixed-point 3D vector conversion
\ usage: <v1> 3s>p
: 3s>p  ( x y z - x. y. z. )  \ convert 3 ints to fixed
  s>p rot s>p rot s>p rot ;

\ intent: fixed-point 3D vector to integer 3D vector conversion
\ usage: <v1.> 3p>s
: 3p>s  ( x. y. z. - x y z )  \ convert 3 fixed to int
  p>s rot p>s rot p>s rot ;

\ intent: round fixed-point value down to nearest integer (result is fixed-point value)
\ usage: <n.> floor
macro: floor  ( n. - n. )  \ round down to integer
  $ffff0000 and ;
