
# File Naming Convention

    system-platform-architecture-purpose.f

## SYSTEMS
  sf                  - SwiftForth
  vfx                 - MPE VFX
  forth94             - Forth94 Compatible

## PLATFORMS
  windows             - Windows Operating Systems
  linux               - Linux Operating Systems

## ARCHITECTURES
  x86                 - Intel 32-bit Architecture
  arm6                - ARM Architecture

## CONFIGURATION CONSTANTS
  the constants in each group are mutually exclusive.

  config-sys-sf       - select the sf system
  config-sys-vfx      - select the vfx system

  config-os-windows   - select the windows platform
  config-os-linux     - select the linux platform

  config-arch-x86     - select the intel architecture
  config-arch-arm6    - select the arm architecture
