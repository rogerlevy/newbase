\ {nodoc} tell the documentation progress monitor tool to ignore this file
ICODE 2! ( x y addr - )  \ documented in: misc.f
  0 [EBP] EAX MOV
  4 [EBP] ECX MOV
  EAX 4 [EBX] MOV
  ECX 0 [EBX] MOV
  8 [EBP] EBX MOV
  12 # EBP ADD
  RET END-CODE

ICODE 2@ ( addr - x y )  \ documented in: misc.f
  4 # EBP SUB
  0 [EBX] EAX MOV
  4 [EBX] EBX MOV
  EAX 0 [EBP] MOV
  RET  END-CODE

ICODE 2+! ( x y addr - )  \ documented in: misc.f
  0 [ebp] eax mov
  4 [ebp] ecx mov
  eax 4 [ebx] add
  ecx 0 [ebx] add
  8 [ebp] ebx mov
  12 # ebp add
  ret end-code

: d+!  ( d addr - )  \ documented in: misc.f
  dup >r d@ d+ r> d! ;

CODE 3! ( x y z addr - )  \ documented in: misc.f
\ Store x,y,z
  0 [ebp] eax mov
  4 [ebp] ecx mov
  eax 8 [ebx] mov
  ecx 4 [ebx] mov
  8 [ebp] eax mov
  eax 0 [ebx] mov
  12 [ebp] ebx mov
  16 # ebp add
  ret end-code

code 3+! ( x y z addr - )  \ documented in: misc.f
  0 [ebp] eax mov
  4 [ebp] ecx mov
  eax 8 [ebx] add
  ecx 4 [ebx] add
  8 [ebp] eax mov
  eax 0 [ebx] add
  12 [ebp] ebx mov
  16 # ebp add
  ret end-code

: 3@  ( addr - x y z )  \ documented in: misc.f
  @+ swap @+ swap @ ;
