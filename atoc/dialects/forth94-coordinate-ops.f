\ {nodoc} tell the documentation progress monitor tool to ignore this file
: 2+  ( x y x y -- x y )  \ documented in: misc.f
  rot + >r  +  r> ;

: 2-  ( x y x y -- x y )  \ documented in: misc.f
  rot swap - >r  -  r> ;

: 3+  ( x y z x y z -- x y z )  \ documented in: misc.f
  rot >r  2+  rot  r> +  -rot ;

: 3-  ( x y z x y z -- x y z ) \ documented in: misc.f
  rot >r  2-  rot  r> -  -rot ;

: xswap  ( x1 y1 x2 y2 -- x2 y1 x1 y2 )  \ documented in: misc.f
  swap rot 2swap ;

: yswap  ( x1 y1 x2 y2 -- x1 y2 x2 y1 )  \ documented in: misc.f
  swap rot ;

: area  ( x y w h - x1 y1 x2 y2 )  \ documented in: misc.f
  2over 2+ ;

: -area  ( x1 y1 x2 y2 - x y w h )  \ documented in: misc.f
  2over 2- ;
