
\ @todo
\ -PATH currently only works on Windows Style paths using \ as a
\ folder delimiter

\ intent: get the extension from a filename
\ usage: s" filename" ending
: ending  ( addr len char - addr len )  \ get the extension from a filename
  >r begin  2dup r@ scan
    ?dup while  2swap 2drop  1 /string
  repeat  r> 2drop ;

\ intent: remove the path from the beginning of a full file path
\ usage: s" fullpath" -path
: -path  ( adr c - adr c )  \ remove the path from the beginning of a full file path
  [char] \ ending [char] / ending 0 max ;

\ intent: remove file extension from the given string
\ usage: s" filename" -ext
: -ext  ( adr c - adr c )  \ remove file extension from given string
  2dup [char] . scan nip 0= ?exit \ no-dot early out
  2dup  [char] . ending  nip -  1-  0 max ;
