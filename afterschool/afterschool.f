\ ==============================================================================
\ ForestLib > GameBlocks [2]
\ GameBlocks library loader [2]
\ ========================= copyright 2014 Roger Levy ==========================

\ compatibility with old code

\ intent: focus current object for remainder of definition using the S register
\ usage: <object> =>
subst with =>  ( addr - ) \ an alias to with for backards-compatibility

\ ------------------------------------------------------------------------------
\ low level stuff
fload stackvecs-2d  \ bring in the 2D stack vector math operations
fload vec           \ bring in the vector math structures
fload box           \ bring in the 2D rectangle (box) data structure
fload color         \ bring in color data structure
fload transform     \ bring in transformation matrix
fload 2array        \ bring in 2D array support
fload vtable        \ bring in vector tables
\ ------------------------------------------------------------------------------

fload assets \ bring in base asset object structure

\ ------------------------------------------------------------------------------
\ allegro dependent stuff
fload allegro5_floatparms_ez  \ bring in 'easy' module for allegro
fload turtlegfx               \ bring in turtle graphics module
fload image                   \ bring in image structure
fload audio-helpers           \ bring in audio helper

fload keyboard-input          \ bring in keyboard input device handler
fload mouse-input             \ bring in mouse input device handler
fload joystick-input          \ bring in joystick input device handler

fload input                   \ bring in general input device handler

fload window                  \ bring in the graphics window
fload colorfx                 \ bring in color blending helper
\ ------------------------------------------------------------------------------

fload gameutils   \ bring in misc game-oriented utility words
fload tweening    \ bring in tween api
fload plane       \ bring in generic tilemap renderer

report( newbase: loaded afterschool )
