\ intent: define an rgba color component
\ usage: %color object <name>
pstruct %color  \ color structure
  1.0 var: rc  \ red component
  1.0 var: gc  \ green component
  1.0 var: bc  \ blue component
  1.0 var: ac  \ alpha component
  
  \ getters n' setters
  macro: red@  .rc @ ;  ( color - n. )
  macro: red!  .rc ! ;  ( n. color - )
  macro: +red  .rc +! ; ( n. color - )
  
  macro: green@  .gc @ ;  ( color - n. )  
  macro: green!  .gc ! ;  ( n. color - )
  macro: +green  .gc +! ; ( n. color - )

  macro: blue@  .bc @ ;  ( color - n. )
  macro: blue!  .bc ! ;  ( n. color - )
  macro: +blue  .bc +! ; ( n. color - )

  macro: alpha@  .ac @ ;  ( color - n. )
  macro: alpha!  .ac ! ;  ( n. color - )
  macro: +alpha  .ac +! ; ( n. color - )
  
  macro: rgba  4@ ;  ( color - r. g. b. a. )
  
  macro: color!  4! ;  ( r. g. b. a. color - ) 
  
  \ intent: sets all color components to zero
  \ usage: <color> color.clear
  macro: color.clear  4 0 ifill ;  ( color - )  \ reset color to zero
  
  \ intent: copy color components from one color object to another
  \ usage: <source color> <destination color> color.copy
  macro: color.copy   4 imove ;  ( src-color dest-color - )  \ clone a color
  
  \ intent: comma the components of a color
  \ usage: <red> <green> <blue> <alpha> color,
  : color,  ( r. g. b. a. )  \ comma a color
    2swap swap , , swap , , ;

  \ darken, lighten
  \ macro: color.scale  dup >r 2@ scale2d r> 2! ;
  \ macro: blend
  
endp

