\ ==============================================================================
\ ForestLib > GameBlocks [2]
\ Tweening [2]
\ ========================= copyright 2014 Roger Levy ==========================

2e fconstant 2e  \ optimization

\ These parameters that you can pass to TWEEN et al transform curves internally.

\ intent: easing will be applied to the start of tween
\ usage: <start> <change> <current> ease-in
[[ noop ]] constant EASE-IN  ( startval change curval - progress )  \ easing at start of tween

\ intent: easing will be applied to the end of tween
\ usage: <start> <change> <current> ease-out
:xt EASE-OUT  ( startval change curval - progress )  \ easing at end of tween
  negate 1.0 + >r  swap over +  swap negate  r> ;

\ intent: easing will be applied to the start and the end of tween
\ usage: <start> <change> <current> ease-in-out
:xt EASE-IN-OUT  ( startval change curval - progress )  \ easing at start and end of tween
  dup 0.5 < if
    2* swap 2/ swap
  else
    swap 2/  rot over +  -rot swap 0.5 -  2*  [ ease-out compile, ]
  then ;

\ intent: easing will not be applied
\ usage: <start> <change> <current> no-ease
:xt NO-EASE  ( startval change curval - progress )  \ no easing will be used
  2drop ;

pstruct %tween  \ tween object data structure
  [i
  noop: 'in/out \ in-out technique
  noop: '!      \ ?
  noop: 'ease   \ easing function
  var interval  \ ?
  var frames    \ ?
  var change    \ ?
  var startval  \ ?
  var addr      \ ?
  var en        \ enabled
  var curval    \ ?
  : addr!  ( - )  \ ?
    addr @ '! @ execute ;
  i]
endp


500 %tween pool tweens \ create pool of tweens

%tween +innards

  \ intent: update the current tween
  \ usage: tween:step
  : tween:step  ( - )  \ update the current tween
    interval @ curval +!
    startval @ change @ curval @ 'in/out @ execute 'ease @ execute addr!
    frames --
    frames @ 0< if
      startval @ change @ + addr!
      en off
      s@ tweens pool-return
    then
  ;

  \ intent: {_INTENT}
  \ usage: {_USAGE}
  : (tween) ( 'ease 'in/out addr startval change #frames '! - )  \ 
    tweens pool-one with
    '! !  1 max 1.0 over / interval ! frames ! change ! startval ! addr ! 'in/out ! 'ease !
    en on  0 curval ! ;

  \ intent: tween a value using easing function
  \ usage: <easer> <in-out-technique> <value> <num frames> tween
  : tween ( 'ease 'in/out destval #frames - )  \ tween value
    >r  >r rot r>  over @ swap over -  r>  ['] !  (tween) ;

  \ intent: remove each tween from tweens
  \ usage: clear-tweens
  : clear-tweens  ( - )  \ empty tweens
    tweens reset-pool ;

  \ intent: call tween:step for each tween in tweens
  \ usage: process-tweens
  : process-tweens  ( - )  \ step each tween
    0 with  [[ s! en @ -exit tween:step ]] tweens each ;

previous


\ Easing functions - all these describe the "in" animations, which are then transformed by EASE-IN etc.
\ exponential formula: c * math.pow(2, 10 * (t / d - 1)) + b;
\ quadratic formula: c * (t /= d) * t + b
:xt LINEAR       ( startval ratio. change. - val )  p* + ;
:xt EXPONENTIAL  ( startval ratio. change. - val )  1.0 - 10.0 p* 2e p>f f**  f>p p* + ;
:xt SINE         ( startval ratio. change. - val )  90.0 p* 90.0 - psin 1.0 + p* + ;
:xt QUADRATIC    ( startval ratio. change. - val )  dup p* p* + ;
:xt CUBIC        ( startval ratio. change. - val )  dup p* dup p* p* + ;
:xt QUARTIC      ( startval ratio. change. - val )  dup p* dup p* dup p* p* + ;
:xt QUINTIC      ( startval ratio. change. - val )  dup p* dup p* dup p* dup p* p* + ;
:xt CIRCULAR     ( startval ratio. change. - val )  dup p* 1.0 swap - psqrt 1.0 - p* negate + ;
: (overshoot)    ( startval ratio. change. - val )  >r dup dup r@ 1.0 + p* r> - p* p* p* + ;
:xt OVERSHOOT    ( startval ratio. change. - val )  1.70158 (overshoot) ;
