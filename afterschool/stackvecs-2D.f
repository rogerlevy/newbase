\ ==============================================================================
\ ForestLib > GameBlocks [2]
\ Stack vector math (2D)
\ ========================= copyright 2014 Roger Levy ==========================

\ intent: compute base2 logarithm of given number
\ usage: <number> log2
: log2  ( n - n )  \ binary logarithm
  1e s>f y*log2(x) f>s ;

\ intent: computer base2 logarithm of given fixed-point number
\ usage: <number.> plog2
: plog2  ( n. - n. )  \ binary logarithm (for fixed-point)
  1e p>f y*log2(x) f>p ;

\ intent: multiply two 2d fixed-point vectors
\ usage: <v1> <v2> 2p*
: 2p*  ( x. y. x. y. - x. y. )  \ multiply vectors
  rot p* >r  p*  r> ;

subst 2p* scale2d  \ friendly alias

\ intent: divide <v1> by <v2> (2d fixed-point vectors) {THIS IS TECHNICALLY NOT A VALID MATHEMATICAL OPERATION}
\ usage: <v1> <v2> 2p/
: 2p/  ( x. y. x. y. - x. y. )  \ divide vectors
  rot swap p/ >r  p/  r> ;

\ intent: get length of vector
\ usage: <v1> magnitude
: magnitude  ( x. y. - n. )  \ get length of vector
  2p>f fdup f* fswap fdup f* f+ fsqrt f>p ;

\ intent: get unit vector from given vector
\ usage: <v1> normalize
: normalize  ( x. y. - x. y. )  \ get unit vector
  2dup .0 d= ?exit  2dup magnitude dup 2p/  1 1 2+ ;

\ intent: uniformly scale vector by a given scalar
\ usage: <v1> <scalar> uscale
: uscale  ( x. y. n. - )  \ uniform scale
  dup 2p* ;

\ intent: get the distance between two vectors
\ usage: <v1> <v2> proximity
macro: proximity  ( x. y. x. y. - n. ) 2swap 2- magnitude ;   \ distance between two vectors

\ intent: rotate a 2d point by given angle (degrees in fixed-point)
\ usage: <v1> <angle.>
: rotate  ( x. y. angle. - x. y. )  \ rotate point
  dup pcos  swap psin  locals| sin(ang) cos(ang) y x |
  x cos(ang) p* y sin(ang) p* -
  x sin(ang) p* y cos(ang) p* + ;

\ intent: get the unit vector from the given angle (degrees in fixed-point)
\ usage: <angle.> uvecfrom
: uvecfrom  ( angle. - x. y. )  \ get unit vector from angle
  >r   r@ pcos  r> psin ;

\ intent: get the unit vector from the given angle (degrees in fixed-point) and scale to given length
\ usage: <length.> <angle.> vecfrom
: vecfrom  ( length. angle. - x. y. )  \ get vector from angle
  uvecfrom  rot  uscale ;

\ intent: get the angle of the given vector
\ usage: <v1> angfrom
: angfrom  ( x. y. - ang. )  \ get angle of vector
  negate p>f p>f fatan2 r>d f>p ;
