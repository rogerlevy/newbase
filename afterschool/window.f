\ ==============================================================================
\ ForestLib > GameBlocks > Allegro5 I/O
\ Graphics window
\ ========================= copyright 2014 Roger Levy ==========================

0 value display       \ handle to allegro window
0 value eventq        \ allegro event queue object
0 value displaytimer  \ allegro timer object
0 value default-font  \ allegro builtin font

\ intent: forces win32 window via handle to be the active window
\ usage: <handle> gotowin
: gotowin  ( winapi-window - )  \ activate win32 window
  dup 1 ShowWindow drop  dup BringWindowToTop drop  SetForegroundWindow drop ;

\ these ensure that allegro's internal keyboard and mouse states are reset.
\ intent: {to be removed}
\ usage: {}
: /kb   ( - )  \ 
  ; \ al_uninstall_keyboard al_install_keyboard drop ;
\ intent: {to be removed}
\ usage: {}
: /mouse  ( - )  \ 
  ; \ al_uninstall_mouse al_install_mouse drop ;

variable (timer)  \ for nesting

\ intent: increment allegro display timer
\ usage: +timer
: +timer  ( - )  \ increment timer
  (timer) ++  displaytimer al_start_timer ;

\ intent: decrement allegro display timer (timer stops at zero)
\ usage: -timer
: -timer  ( - )  \ decrement timer
  (timer) --  (timer) @ 0 = if  displaytimer al_stop_timer  then ;

\ intent: force allegro display window to take focus
\ usage: >gfx
: >gfx  ( - )  \ force allegro display window to take focus
  \ /kb /mouse
  display al_get_win_window_handle  gotowin  ;

\ intent: force sf ide window to take focus for faster development iteration
\ usage: >ide
: >ide  ( - )  \ force sf ide to take focus
  HWND gotowin  ;

create res  640 , 480 ,  \ desired screen resolution

variable fps  60 fps !  \ frames per second of main loop

\ intent: initialize allegro and addon libraries; create display and init allegro objects
\ usage: {not used externally}
: (init-allegro)  ( - )  \ init allegro and addons
  al_init                   not if  " INIT-ALLEGRO: Couldn't initialize Allegro." alert     -1 abort then
  al_init_image_addon       not if  " Allegro: Couldn't initialize image addon." alert      -1 abort then
  al_init_primitives_addon  not if  " Allegro: Couldn't initialize primitives addon." alert -1 abort then
  al_init_font_addon        not if  " Allegro: Couldn't initialize font addon." alert       -1 abort then
  al_install_mouse          not if  " Allegro: Couldn't initialize mouse." alert            -1 abort then
  al_install_keyboard       not if  " Allegro: Couldn't initialize keyboard." alert         -1 abort then
  al_install_joystick       not if  " Allegro: Couldn't initialize joystick." alert         -1 abort then

  ALLEGRO_VSYNC 1 ALLEGRO_SUGGEST  al_set_new_display_option
  res 2@  al_create_display  to display

  al_create_builtin_font to default-font

  \ initialize timer and event queue
  al_create_event_queue  to eventq
  1e fps @ s>f f/  al_create_timer  to displaytimer

  \ attach event sources
  eventq  displaytimer  al_get_timer_event_source  al_register_event_source
  eventq  display  al_get_display_event_source  al_register_event_source
  eventq  al_get_mouse_event_source  al_register_event_source
  eventq  al_get_keyboard_event_source  al_register_event_source
  eventq  al_get_joystick_event_source  al_register_event_source

  report" Allegro window and subsystems initialized"

  >ide
;

\ selecting 3.0 fixes the fullscreen switching bug that once hounded me.
ALLEGRO_OPENGL_3_0 constant common-allegro-flags

\ intent: init allegro with windowed display flags
\ usage: init-allegro
: init-allegro  ( - )  \ init allegro windowed display
  common-allegro-flags
  ALLEGRO_WINDOWED or
  ALLEGRO_RESIZABLE or
  al_set_new_display_flags  (init-allegro) ;

\ intent: init allegro with fullscreen display flags
\ usage: init-allegro-fullscreen
: init-allegro-fullscreen  ( - )  \ init allegro fullscreen display
  common-allegro-flags
  ALLEGRO_FULLSCREEN_WINDOW or
  al_set_new_display_flags  (init-allegro) ;

\ intent: copy render buffers to screen so that changes can be seen
\ usage: show-frame
: show-frame  ( - )  \ flip display buffers
  al_flip_display ; \ al_wait_for_vsync ;

\ intent: shutdown allegro subsystems and cleanup before exiting
\ usage: close-allegro
: close-allegro  ( - )  \ shutdown allegro
  display -exit
  displaytimer  al_destroy_timer
  eventq  al_destroy_event_queue
  display  al_destroy_display
  al_uninstall_system
  0 to display ;

\ ensure allegro gets closed when the program finishes execution
:onexit  close-allegro ;

\ {RM: This is a little project of mine Roger. Don't change this or take over. It's my own challenge ;) }
\ create wsz 4 cells /allot
\ : center-window  ( - )  \ centers graphics window on the primary screen
\  display >r
\  0 GetSystemMetrics  1 GetSystemMetrics
  \ sw sh
\  res 2@
  \ sw sh ww wh
\  swap 2 /  swap 2 /  2-  al_set_window_position ;
\ center-window

0 40 al_set_new_window_position  \ set the position of the window to a more friendly location than 0,0
