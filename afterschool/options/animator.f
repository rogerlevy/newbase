\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ ==============================================================================
\ ForestLib
\ Animator object (WIP)
\ ========================= copyright 2014 Roger Levy ==========================

\ {RL} This class may or may not be used.  It's kind of an experimental thing.

\ animation format:
\    (frame data - default is 4 cells for bitmap source region x,y,w,h)
\    ... more frames ...
\    -1, address (end, automatically loops to address )

\ frame size can easily be changed by setting FRAMESIZE (there really isn't a need to subclassspeed)

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %animator  \ {brief}
  0.25 var: animator-speed  \ {brief}
  var animptr               \ {brief}
  var animctr               \ {brief}
  var looped                \ {brief}
  4 cells var: framesize    \ {brief}
endp

\ intent: {INTENT}
\ usage: {USAGE}
: animstep  ( - )  \ {brief}
  animptr @ -exit
  looped off
  animator-speed @ animctr +!
  begin animctr @ 1.0 >= while
    animctr @ $0000FFFF and animctr !
    framesize @ animptr +!
    animptr @ @ -1 = if
      looped on
      animptr @ cell+ @ animptr !
    then
  repeat ;

\ intent: {INTENT}
\ usage: {USAGE}
: play-animation  ( animation animator - )  \ {brief}
  with  animptr !  looped off ;

\ intent: {INTENT}
\ usage: {USAGE}
: stop-animating  ( animator - )  \ {brief}
  with  animptr off  looped off ;
