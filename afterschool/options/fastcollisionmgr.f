\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ ==============================================================================
\ ForestLib
\ Fast collision manager object.  Does efficient AABB collision checks of massive
\ numbers of rectangles.
\ ========================= copyright 2014 Roger Levy ==========================

\ Ask {RL} to describe this in detail

\ intent: {INTENT}
\ usage: {USAGE}
0 value fcmgr  \ current fcmgr

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %cgridbox  \ {brief}
  [i
  cell field x   \ {brief}
  cell field y   \ {brief}
  cell field a   \ right
  cell field b   \ bottom
  cell field s1  \ sector 1
  cell field s2  \  ...
  cell field s3  \  ...
  cell field s4  \  ...
  i]
endp

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %fcmgr  addressing: fcmgr + ;  \ {brief}
  prototype to fcmgr

  %cgridbox +innards

  [i
  7 constant bitshift      \ {brief}
  $ffffff80 constant mask  \ {brief}
  i]

  [i
    \ used by RESET-FCMGR
    128 var: cols          \ {brief}
    128 var: rows          \ {brief}
    128 constant sectw     \ {brief}
    128 constant secth     \ {brief}

  \ these vars and constants aren't needed for getting a sector from a coordinate - bitshift and mask take care of it
  \ grid size and "granularity" is fixed.  128x128 squares each 128x128 in size.
  \ - the effective square size can be made smaller by left-bitshifting the 2 coordinates of all the cgridbox's
  \  added to it.  dividing by 2 (bitshift left 1) would give a virtual square size of 64x64, useful for bullet hell or particles.
  i]

  [i
    var sectors         \ {brief}
    var links           \ {brief}
    var i.link          \ points to structure in links:  link to next ( i.link , box , )
    var lastsector      \ {brief}
    variable topleft    \ {brief}
    variable topright   \ {brief}
    variable btmleft    \ {brief}

    \ intent: {INTENT}
    \ usage: {USAGE}
    : sector  ( x y - addr )  \ {brief}
      mask and swap bitshift >> + cells sectors @ + ;

    \ intent: {INTENT}
    \ usage: {USAGE}
    : link  ( box sector - )  \ {brief}
      >r
      i.link @ cell+ !  \ 1. link box
      r@ @ i.link @ !  \ 2. link the i.link to address in sector
      i.link @ r> !   \ 3. store current link in sector
      2 cells i.link +! ;  \ 4. and increment current link

    \ not the best algorithm ....
    \ to do it right, we have to check against all previous sectors checked not just the direct last one.
    : ?corner  ( x y - 0 | sector )  \ see what sector the given coords are in / cull already-checked corners
      sector  dup lastsector @ = if  drop 0  exit  then  dup lastsector ! ;

  i]

  \ intent: {INTENT}
  \ usage: {USAGE}
  : box>box?  ( box1 box2 - box1 box2 flag )  \ {brief}
    2dup = if  false  exit  then \ boxes can't collide with themselves!
    2dup  >r  4@  r> 4@  overlap? ;

  defer grid-collide  ' 2drop is grid-collide  ( cgridbox1 cgridbox2 - ) \ box1 is the passed in box

  [i

  \ intent: {INTENT}
  \ usage: {USAGE}
  : box>sector  ( cgridbox1 sector - )  \ {brief}
    swap >r
    begin @ dup while
      dup cell+ @  r@  swap  box>box? if  grid-collide  else  2drop  then
    repeat
    r> drop drop ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : ?box>sector  ( cgridbox1 sector|0 - )  \ check a cgridbox against a sector
    ?dup if  box>sector  else  drop  then ;

  i]

  \ intent: {INTENT}
  \ usage: {USAGE}
  : reset-fcmgr ( fcmgr=fcmgr;  - )  \ {brief}
    sectors @ cols 2@ * ierase
    links @ i.link ! ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : add-box  ( fcmgr=fcmgr; cgridbox - )  \ {brief}
    ( box ) with
    x @ y @ ?corner ?dup if  dup s1 !  s@ swap link  else  s1 off  then
    a @ y @ ?corner ?dup if  dup s2 !  s@ swap link  else  s2 off  then
    x @ b @ ?corner ?dup if  dup s4 !  s@ swap link  else  s4 off  then
    a @ b @ ?corner ?dup if  dup s3 !  s@ swap link  else  s3 off  then
    ( topleft off topright off btmleft off ) lastsector off ;

  \ note: box must be added before checking against the grid, as adding the box
  \ determines which sector(s) it is in.
  : check-grid  ( fcmgr=fcmgr; cgridbox1 - )  \ {brief}
    dup dup .s1 @ ?box>sector
    dup dup .s2 @ ?box>sector
    dup dup .s3 @ ?box>sector
    dup .s4 @ ?box>sector ;

  [i
  : corners  ( cgridbox - )  \ {brief}
    with
    x @ y @ ?corner
    a @ y @ ?corner
    x @ b @ ?corner
    a @ b @ ?corner lastsector off
    ;
  i]

  \ this version doesn't require the box to be added beforehand
  : check-box  ( fcmgr=fcmgr; cgridbox1 - )  \ {brief}
    dup >r  corners  r@ swap  ?box>sector
      r@ swap ?box>sector
      r@ swap ?box>sector
      r> swap ?box>sector ;

  : fcmgr!  ( fcmgr - )  \ {brief}
    to fcmgr ;

  : fcmgr,  ( maxboxes - )  \ {brief}
    %fcmgr instantiate fcmgr!
    here sectors !  128 128 * cells /allot
    here links !    4 * 2 cells * /allot ;

  : fcmgr.width  ( fcmgr - w )  \ {brief}
    cols @ sectw * ;

  : fcmgr.height  ( fcmgr - h )  \ {brief}
    rows @ secth * ;

  %cgridbox -innards
endp
