\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ this file is semi-experimental

\ intent: {INTENT}
\ usage: {USAGE}
%image ~> %font  \ {brief}
  var alfnt   \ {brief}
  var alfnth  \ {brief}

  \ intent: {INTENT}
  \ usage: {USAGE}
  : font:onload  ( #ranges  addr  filename count - )  \ {brief}
    image:onload
    albmp @ -rot al_grab_font_from_bitmap alfnt !
    alfnt @ al_get_font_line_height alfnth ! ;

  ' font:onload prototype .onload !

endp

\ intent: {INTENT}
\ usage: {USAGE}
: font, ( adr c - )  \ {brief}
  %font instance with asset^ ;

\ intent: {INTENT}
\ usage: {USAGE}
: font  ( - <name> <filespec> )  \ {brief}
  ?create namespec font, ;

\ intent: {INTENT}
\ usage: {USAGE}
create a4ranges  \ {brief}
  $0020 , $007F ,  \ ASCII
  $00A1 , $00FF ,  \ Latin 1
  $0100 , $017F ,  \ Extended-A
  $20AC , $20AC ,  \ Euro

4 a4ranges  font  a4.font a4_font.png  \ {brief}

\ intent: {INTENT}
\ usage: {USAGE}
a4.font value fnt  \ current font

\ intent: {INTENT}
\ usage: {USAGE}
: font>  ( - ALLEGRO_FONT )  \ {brief}
  fnt .alfnt @ ;

\ intent: {INTENT}
\ usage: {USAGE}
: text ( adr c - )  \ draw some text using current font
  batch[  z$ font> ALLEGRO_ALIGN_LEFT rot al-draw-text  ]batch ;

\ intent: {INTENT}
\ usage: {USAGE}
: strwidth  ( adr c - w )  \ {brief}
  z$ font> swap al_get_text_width ;

\ intent: {INTENT}
\ usage: {USAGE}
macro: fontheight  ( - h )  \ {brief}
  fnt .alfnth @ ;  \ get current font height
