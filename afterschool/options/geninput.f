\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ ==============================================================================
\ ForestLib > GameBlocks
\ Generic input (a.k.a. input mapping) (working, but lacks axis-to-button mapping)
\ ========================= copyright 2014 Roger Levy ==========================

package geninput public

\ wordset for setting up generic inputs ... i.e. "action", "cancel", "up"...
\ you set up "mappings" which tell the event handler where to route various inputs to in your struct.
\ mapping cells contain indexes that map to vars in a state struct.
\ additionally, because different joysticks should be distinguished, and the keyboard and the mouse could
\  potentially be used as their own "joysticks" the mapping counts each input device separately (including joysticks)
\  this way, you can set up an array of structs and the devices can route to each s@ individually,
\  or you could combine any number of devices and they would control the same state struct.
\ you can also setup generic input event handlers, which is important for making sure that no event is missed.


\ for now, keepin' it basic.  but later it will get more sophisticated, with:
\  - ability to have more than one concurrent mapping and copy/erase them easily
\  - mapping thumbsticks and the mouse's movement
\  - ability to map joysticks separately - for now, we treat them all the same.

\ mapping sources currently supported:
\  keys
\  joystick buttons
\  mouse buttons


\ mapping struct
\  each source is a button or a key, and the routing destination is an adress to an inputevent state


\ input state/event struct
\  value
\  device
\  delta  ( for detecting if pressed, released, increased, decreased... )
\  timestamp
\  callback  ( s@ -> statestruct )

\ defining the mapping - routines:
\  * create an input event subst "action", "cancel", "up", "down"
\  * assign trigger for mouse, keyboard, and all joysticks at once
\  * assign trigger for a specific device ( joystick = all joysticks )
\  * clear the mapping
\  * callback: check if pressed or released

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %inputmap  \ {brief}
  256 cells field keymap
  _AL_MAX_JOYSTICK_BUTTONS cells 16 * field joymap
  32 cells field mousemap
endp

create mapping  %inputmap instance,  \ {brief}

\ device constants
0
enum dvc-joystick  \ {brief}
enum dvc-keyboard  \ {brief}
enum dvc-mouse     \ {brief}
drop

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %inputevent  \ {brief}
  var val  var device  var trigger#  var delta  dvar timestamp  noop: callback
endp

0 value giq  \ {brief}

private
create event  256 allot  \ {brief}
public

\ intent: {INTENT}
\ usage: {USAGE}
: init-generic-input  ( - )  \ {brief}
  al_create_event_queue to giq
  giq al_get_mouse_event_source al_register_event_source
  giq al_get_keyboard_event_source al_register_event_source
  giq al_get_joystick_event_source al_register_event_source ;

\ for each event type,
\  determine value, device, delta, and timestamp
\  from device, find offset into mapping
\  if joystick, also add to offset based on joystick # -
\  unfortunately this involves searching the joyhandles stack to find the index.

\ intent: {INTENT}
\ usage: {USAGE}
: type>  ( - allegro_event_type )  \ {brief}
  event .ALLEGRO_EVENT_TYPE-type @ ;

create deltas  \ {brief}
  0 , 0 , 1 , -1 , 0 , 0 , 0 , 0 , 0 , 0 ,
  1 , 0 , -1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
  0 , 1 , -1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,

\ intent: {INTENT}
\ usage: {USAGE}
: delta>  ( - delta )  \ get delta from table according to event type
  type> cells deltas + @ ;

\ #define  ALLEGRO_EVENT_JOYSTICK_AXIS            1
\ #define  ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN       2
\ #define  ALLEGRO_EVENT_JOYSTICK_BUTTON_UP        3
\ #define  ALLEGRO_EVENT_JOYSTICK_CONFIGURATION      4
\ #define  ALLEGRO_EVENT_KEY_DOWN              10
\ #define  ALLEGRO_EVENT_KEY_CHAR              11
\ #define  ALLEGRO_EVENT_KEY_UP                12
\ #define  ALLEGRO_EVENT_MOUSE_AXES             20
\ #define  ALLEGRO_EVENT_MOUSE_BUTTON_DOWN        21
\ #define  ALLEGRO_EVENT_MOUSE_BUTTON_UP          22

create maplocs  \ {brief}
  0 .joymap ,
  0 .keymap ,
  0 .mousemap ,

\ intent: {INTENT}
\ usage: {USAGE}
: joyindex>  ( - n )  \ {brief}
  -1  0  event .ALLEGRO_JOYSTICK_EVENT-*id @  locals| needle index found |
  [[ @ needle = if  index to found  then  1 +to index ]] joyhandles each  found ;

\ intent: {INTENT}
\ usage: {USAGE}
: allegro-device-category  ( - category# )  \ get category# according to event type
  type> 10 / ;

\ intent: {INTENT}
\ usage: {USAGE}
: joystick?  ( - flag )  \ {brief}
  allegro-device-category 0= ;

\ intent: {INTENT}
\ usage: {USAGE}
: th  ( adr n - adr )  \ {brief}
  cells + ;

\ intent: {INTENT}
\ usage: {USAGE}
: device>  ( - device# ) \ see above
  allegro-device-category joystick? if  joyindex> +  then ;

\ intent: {INTENT}
\ usage: {USAGE}
: mapping>  ( - adr )  \ {brief}
  mapping maplocs allegro-device-category th @ + ;
  \ allegro-device-category joystick? if
  \   joyindex> [ _AL_MAX_JOYSTICK_BUTTONS cells ]# * +
  \ then ;

\ intent: {INTENT}
\ usage: {USAGE}
: timestamp>  ( - d )  \ {brief}
  event .ALLEGRO_EVENT_TYPE-timestamp d@ ;

create mapping-offsets
  0 .ALLEGRO_JOYSTICK_EVENT-button ,
  0 .ALLEGRO_KEYBOARD_EVENT-keycode ,
  0 .ALLEGRO_MOUSE_EVENT-button ,

\ intent: {INTENT}
\ usage: {USAGE}
: trigger> ( - n )  \ {brief}
  mapping-offsets  allegro-device-category th @  event + @ ;

\ intent: {INTENT}
\ usage: {USAGE}
: retrigger  ( inputevent - )  \ {brief}
  with  callback @ execute ;

\ intent: {INTENT}
\ usage: {USAGE}
0 value ievt  \ current input event

\ intent: {INTENT}
\ usage: {USAGE}
: trigger  ( device trigger# delta inputevent - )  \ {brief}
  with  dup val +!  delta !  trigger# !  device !  s@ to ievt  callback @ execute ;

\ intent: {INTENT}
\ usage: {USAGE}
: filter  ( - )  \ {brief}
  type> >r
  r@ ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN =
  r@ ALLEGRO_EVENT_JOYSTICK_BUTTON_UP = or
  r@ ALLEGRO_EVENT_KEY_DOWN = or
  r@ ALLEGRO_EVENT_KEY_UP = or
  r@ ALLEGRO_EVENT_MOUSE_BUTTON_DOWN = or
  r> ALLEGRO_EVENT_MOUSE_BUTTON_UP = or ;

\ intent: {INTENT}
\ usage: {USAGE}
: poll-generic-inputs  ( - )  \ {brief}
  giq al_is_event_queue_empty not if
    begin giq event al_get_next_event while
      filter if
        mapping> trigger> th @  ?dup if
          >r  device> trigger> delta>  r@ trigger
          timestamp>  r> .timestamp d!
        then
      then
    repeat
  then ;


\ intent: {INTENT}
\ usage: {USAGE}
: pressed   ( inputevent - flag )  \ {brief}
  .delta @ 0 > ;
\ intent: {INTENT}
\ usage: {USAGE}
: released  ( inputevent - flag )  \ {brief}
  .delta @ 0 < ;
\ intent: {INTENT}
\ usage: {USAGE}
: pressed?  ( - flag )  \ {brief}
  ievt pressed ;
\ intent: {INTENT}
\ usage: {USAGE}
: released? ( - flag )  \ {brief}
  ievt released ;
\ intent: {INTENT}
\ usage: {USAGE}
: ival  ( inputevent - value )  \ {brief}
  .val @ ;

\ intent: {INTENT}
\ usage: {USAGE}
: unassign  ( inputevent - )  \ {brief}
  ['] noop swap .callback ! ;

\ intent: {INTENT}
\ usage: {USAGE}
: assign  ( xt inputevent - )  \ {brief}
  .callback ! ;

create inputevents 256 stack,  \ {brief}

\ intent: {INTENT}
\ usage: {USAGE}
: input:  ( - <name> )  \ {brief}
  here with  %inputevent instance,  :noname s@ assign   s@ inputevents push ;

\ intent: {INTENT}
\ usage: {USAGE}
: map   ( inputevent n adr - )  \ {brief}
  swap th ! ;
\ intent: {INTENT}
\ usage: {USAGE}
: unmap  ( inputevent n adr - )  \ {brief}
  swap th off ;

\ all the following use the "global" mapping.  (temporary?)

\ intent: {INTENT}
\ usage: {USAGE}
: [key]    ( - adr )  \ {brief}
  mapping ;
\ intent: {INTENT}
\ usage: {USAGE}
: [mouse]  ( - adr )  \ {brief}
  mapping .mousemap ;  \ not for mouse buttons it starts at 1 (lb=1,rb=2,mb=3)
\ intent: {INTENT}
\ usage: {USAGE}
: [joy]    ( - adr )  \ {brief}
  mapping .joymap ;

\ intent: {INTENT}
\ usage: {USAGE}
: mapkj  ( inputevent key-const joy-const - )  \ {brief}
  locals| joy key evt |
  evt key [key] map
  evt joy [joy] map ;

\ intent: {INTENT}
\ usage: {USAGE}
: mapkjm  ( inputevent key-const joy-const mouse - )  \ {brief}
  locals| mouse joy key evt |
  evt mouse [mouse] map
  evt key [key] map
  evt joy [joy] map ;

\ intent: {INTENT}
\ usage: {USAGE}
: clear-mapping  ( - )  \ {brief}
  mapping [ %inputmap sizeof ]# erase ;

end-package
