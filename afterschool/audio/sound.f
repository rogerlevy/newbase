\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ ==============================================================================
\ ForestLib > GameBlocks
\ Basic sound engine (working, but also a WIP)
\ ========================= copyright 2014 Roger Levy ==========================

\ sound object
\  abstracts samples, file streams, and dsp streams
\  allows ramped (tweened) changes of volume, pan, pitch

0 value snd  \ {brief}

\ intent: {INTENT}
\ usage: {USAGE}
: snd!  ( sound - )  \ {brief}
  to snd ;

ALLEGRO_PLAYMODE_ONCE constant once       \ {brief}
ALLEGRO_PLAYMODE_LOOP constant looping    \ {brief}
ALLEGRO_PLAYMODE_BIDIR constant bidir     \ {brief}

\ floating point tween controller
\  has a destination, speed, destination value, and current value
\  TODO: tweening curves

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %ftweener  \ {brief}
  0e sfvar: fval      \ {brief}
  0e sfvar: fsrcval   \ {brief}
  0e sfvar: fdestval  \ {brief}
  0e sfvar: fstep     \ timestep 10ms
  var ftweenflags     \ 1 = enable
  noop: onftweenend
endp


\ intent: {INTENT}
\ usage: {USAGE}
%asset ~> %sample  \ {brief}
  var alsmp        \ {brief}
  var smprate      \ {brief}

  \ intent: {INTENT}
  \ usage: {USAGE}
  : sample:onload  ( adr c - )
   z$ al_load_sample  dup 0= abort" Error in SAMPLE:ONLOAD : There was a problem loading a sample."
    alsmp ! ;

  ' sample:onload onload !
endp

\ intent: {INTENT}
\ usage: {USAGE}
: sample,  ( - )  \ {brief}
  %sample build> asset^ ;

\ intent: {INTENT}
\ usage: {USAGE}
: sound-fields  ( - )  \ {brief}
  [[ create  over ,  +  does> @ snd + ]] is create-ifield  prototype snd! ;

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %sound  \ {brief}
  [[ sound-fields ]] fields

  var handle       \ {brief}
  var playmode     \ {brief}
  var juststarted  \ {brief}

  var sndsmp       \ {brief}

  create sndvt-root 10 vtable,  \ {brief}
  sndvt-root var: sndvt         \ {brief}
  [[ @ sndvt @ ]] sndvt-root set-vtable-fetcher                 \ {brief}
  2 0 action (vol!)       ( handle sfloat )                     \ {brief}
  2 0 action (pan!)       ( handle sfloat )                     \ {brief}
  2 0 action (spd!)       ( handle sfloat )                     \ {brief}
  2 0 action (playmode!)  ( handle mode - )                    \ {brief}
  2 0 action (seek)       ( handle n. - ) \ secs fixed         \ {brief}
  1 1 action (pos)        ( handle - n. ) \ secs fixed         \ {brief}
  1 1 action (length)     ( handle - n ) \ unit depends (???)  \ {brief}
  1 1 action (finished)   ( handle - flag )                    \ {brief}
  2 0 action (playstate!) ( handle flag - )                    \ {brief}
  1 0 action (kill)       ( handle - )                         \ {brief}

  [[ prototype >sndvt @ vbeget  prototype >sndvt ! ]] begetting

  %ftweener inline: sndvol  1e sndvol sf!    \ {brief}
  %ftweener inline: sndpan  0.0e sndpan sf!  \ -1.0e is left, 1.0e is right.
  %ftweener inline: sndspd  1e sndspd sf!    \ {brief}
  0e sfvar: tempvol                          \ {brief}
  0e sfvar: tempspd                          \ {brief}
\  var sipa \ sample instance pointer address
\  noop: onsndloop
\  noop: onsndend
  [[ sound-fields  sndvt @ to vt ]] fields
endp


\ : smpinst>  sipa @ @ ;


create sounds      #maxsmp %sound sizeof array,                         \ {brief}
create freesounds  #maxsmp stack,  [[ freesounds push ]] sounds each  \ {brief}

\ intent: {INTENT}
\ usage: {USAGE}
: smpinst>  ( - sample-instance )  \ {brief}
  snd  sounds >items -  %sound sizeof /  sample-instances [] @ ;

\ intent: {INTENT}
\ usage: {USAGE}
: onesound     ( prototype - sound )  \ {brief}
  freesounds length 0= abort" Exceeded maximum sounds. TODO: add sound-stealing or something"
  freesounds pop  swap over initialize ;

\ intent: {INTENT}
\ usage: {USAGE}
: sound:delete  ( - )  \ {brief}
  handle @ (kill)  handle off  snd freesounds push  report" sound deleted " ;

\ intent: {INTENT}
\ usage: {USAGE}
: clear-sounds  ( - )  \ {brief}
  [[ snd!  handle @ if  sound:delete  then ]] sounds each ;

\ intent: {INTENT}
\ usage: {USAGE}
%sound ~> %streamsound  \ {brief}
  :: (vol!)       al_set_audio_stream_gain drop ;
  :: (pan!)       al_set_audio_stream_pan drop ;
  :: (spd!)       al_set_audio_stream_speed drop ;
  :: (playmode!)  dup playmode !  al_set_audio_stream_playmode drop ;
  :: (seek)       ( seconds. ) p>s 0 al_seek_audio_stream_secs drop ;
  :: (pos)        al_get_audio_stream_position_secs ;
  :: (length)     ( - seconds ) al_get_audio_stream_length_secs ;
  :: (playstate!) al_set_audio_stream_playing drop ;
  :: (finished)   playmode @ once <> if  drop 0  exit  then  dup al_get_audio_stream_position_secs  swap al_get_audio_stream_length_secs >= ;
  :: (kill)       al_destroy_audio_stream ;
endp

\ intent: {INTENT}
\ usage: {USAGE}
: smprate@  ( - n )  \ {brief}
  handle @ al_get_sample_instance_frequency  dup smprate ! ;

\ intent: {INTENT}
\ usage: {USAGE}
: samps>psec  ( #samples - sec. )  \ {brief}
  s>f smprate@ s>f f/ f>p ;

\ intent: {INTENT}
\ usage: {USAGE}
: psec>samps  ( sec. - #samples )  \ {brief}
  p>f smprate@ s>f f* f>s ;

\ intent: {INTENT}
\ usage: {USAGE}
%sound ~> %samplesound  \ {brief}
  :: (vol!)       al_set_sample_instance_gain drop ;
  :: (pan!)       al_set_sample_instance_pan drop ;
  :: (spd!)       al_set_sample_instance_speed drop ;
  :: (playmode!)  dup playmode !  al_set_sample_instance_playmode drop ;
  :: (seek)       ( seconds. ) psec>samps al_set_sample_instance_position drop ;
  :: (pos)        ( - seconds. ) al_get_sample_instance_position samps>psec ;
  :: (length)     ( - seconds. ) al_get_sample_instance_length samps>psec ;
  :: (playstate!) al_set_sample_instance_playing drop ;
  :: (finished)   playmode @ once <> if  drop 0  exit  then  al_get_sample_instance_position 0 <= ;
  :: (kill)       false (playstate!) ;
endp

\ intent: {INTENT}
\ usage: {USAGE}
: playing!  ( flag - )  \ {brief}
  handle @  swap (playstate!) ;

\ intent: {INTENT}
\ usage: {USAGE}
: sndlength@  ( - n )  \ {brief}
  handle @ (length) ;

\ intent: {INTENT}
\ usage: {USAGE}
: soundlength  ( sound - n )  \ {brief}
  snd! sndlength@ ;

\ intent: {INTENT}
\ usage: {USAGE}
: sound:update  ( - )  \ {brief}
  handle @ dup sndvol @ (vol!)
        dup sndpan @ (pan!)
           sndspd @ (spd!)  ;

\ intent: {INTENT}
\ usage: {USAGE}
: soundpos@  ( sound - n )  \ {brief}
  snd! handle @ (pos) ;

\ temporarily doesn't support ramps...
\ intent: {INTENT}
\ usage: {USAGE}
: volramp  ( src. dest. speed. - )  \ {brief}
  drop nip p>f 1sf sndvol !  sound:update ;
\ intent: {INTENT}
\ usage: {USAGE}
: panramp  ( src. dest. speed. - )  \ {brief}
  drop nip p>f 1sf sndpan !  sound:update ;
\ intent: {INTENT}
\ usage: {USAGE}
: spdramp  ( src. dest. speed. - )  \ {brief}
  drop nip p>f 1sf sndspd !  sound:update ;

\ intent: {INTENT}
\ usage: {USAGE}
: sndvol!  ( n. - )  \ {brief}
  0 swap 0 volramp ;
\ intent: {INTENT}
\ usage: {USAGE}
: sndpan!  ( n. - )  \ {brief}
  0 swap 0 panramp ;
\ intent: {INTENT}
\ usage: {USAGE}
: sndspd!  ( n. - )  \ {brief}
  0 swap 0 spdramp ;

\ intent: {INTENT}
\ usage: {USAGE}
: seek ( n sound - )  \ {brief}
  snd!  handle @ swap (seek) ;

\ intent: {INTENT}
\ usage: {USAGE}
: reversed  ( - )  \ {brief}
  bidir (playmode!)  sndlength@ (seek) ;  \ might not work

\ : playmode ( n ) handle @ swap (playmode!) 0 playing! 0 seek 1 playing! ;

\ intent: {INTENT}
\ usage: {USAGE}
: sndpause  ( - )  \ {brief}
  sndvol sf@ tempvol sf!  0e sndvol sf!
  sndspd sf@ tempspd sf!  0e sndspd sf!
  sound:update ;

\ intent: {INTENT}
\ usage: {USAGE}
: sndresume  ( - )  \ {brief}
  tempvol sf@ 1sf sndvol !
  tempspd sf@ 1sf sndspd !
  sound:update ;

\ intent: {INTENT}
\ usage: {USAGE}
: playsample ( sample playmode - )  \ {brief}
  >r %samplesound onesound snd!
  smpinst> handle !
  >alsmp @  dup sndsmp !
  ( alsmp ) handle @ swap al_set_sample drop
  handle @ mixer al_attach_sample_instance_to_mixer drop
  handle @ r> (playmode!)
  handle @ al_play_sample_instance drop
  10 juststarted !
  ; \ 1 playing! ;

\ intent: {INTENT}
\ usage: {USAGE}
: playstream ( path c playmode - )  \ {brief}
  >r %streamsound onesound snd!
  z$ 2 4096 al_load_audio_stream handle !
  handle @ r> (playmode!)
  handle @ mixer al_attach_audio_stream_to_mixer drop
  10 juststarted !
  ;

\ intent: {INTENT}
\ usage: {USAGE}
: stopsound  ( sound - )  \ {brief}
  snd!  handle @ -exit  sound:delete ;

\ intent: {INTENT}
\ usage: {USAGE}
: sample  ( - <name> <path> ) ( playmode - )  \ {brief}
  ?create  namespec sample,  does> swap playsample ;

\ intent: {INTENT}
\ usage: {USAGE}
: stream  ( - <name> <path> ) ( playmode - )  \ {brief}
  create  namespec string,  does> count find-asset  rot  playstream ;

\ intent: {INTENT}
\ usage: {USAGE}
: ?done  ( - flag )  \ {brief}
  handle @ (finished)  juststarted --  juststarted @ 0 <= and  ( flag )  dup if  sound:delete  then ;

\ intent: {INTENT}
\ usage: {USAGE}
: sound:step  ( - )  \ {brief}
  ?done ?exit
  \ TODO: process ramps
  sound:update
;

\ intent: {INTENT}
\ usage: {USAGE}
: update-sounds  ( - )  \ {brief}
  snd  [[ snd!  handle @ if  sound:step  then ]] sounds each  snd! ;

\ --------------------------------------------------------------------------------------
\ Fuck this!!!!!!

\ 1024 1024 * 4 * task audiotask

\ : /audiomngr
\   audiotask activate init-audio begin 1 Sleep drop [c update-sounds c] again
\ ;

\ variable soundframes
\ [[ 2drop [c init-audio c] begin soundframes ++ 1 sleep drop [c update-sounds c] again ]] 2 cb: mycb

\ 0 value t
\ --------------------------------------------------------------------------------------

\ intent: {INTENT}
\ usage: {USAGE}
: close-sound  ( - )  \ {brief}
  ; \ t al_destroy_thread ;

\ intent: {INTENT}
\ usage: {USAGE}
: init-sound  ( - )  \ {brief}
  init-audio
  \ /audiomngr
  ; \ mycb 0 al_create_thread dup to t al_start_thread ;

\ /audiomngr

\ intent: {INTENT}
\ usage: {USAGE}
: pause-sounds  ( - )  \ {brief}
  snd  [[ snd!  handle @ if  sndpause  then ]] sounds each  snd! ;

\ intent: {INTENT}
\ usage: {USAGE}
: resume-sounds  ( - )  \ {brief}
  snd  [[ snd!  handle @ if  sndresume  then ]] sounds each  snd! ;

\ intent: {INTENT}
\ usage: {USAGE}
: mastervol!  ( n. - )  \ {brief}
  mixer swap p>f 1sf al_set_mixer_gain drop ;

\ intent: {INTENT}
\ usage: {USAGE}
: -sounds  ( - )  \ {brief}
  mixer 0e 1sf al_set_mixer_gain drop ;

\ intent: {INTENT}
\ usage: {USAGE}
: +sounds  ( - )  \ {brief}
  mixer 1e 1sf al_set_mixer_gain drop ;
