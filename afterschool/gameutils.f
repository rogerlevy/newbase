\ ==============================================================================
\ ForestLib > GameBlocks [2]
\ Misc. game related words [2]
\ ========================= copyright 2014 Roger Levy ==========================

\ ------------------------------------------------------------------------------
\ Matrix stuff

\ intent: for use by 2X , TRANSLATE ...
create mymatrix 8 16 * allot  \ temporary matrix

\ intent: upscale following rendering operations by 2
\ usage: 2X  ( and then draw some stuff )
: 2x  ( - )  \ render everything following at 200% scale
  mymatrix al_identity_transform
  mymatrix 2e 2e al_scale_transform
  mymatrix al_use_transform ;

\ intent: translate following rendering operations by given X and Y offsets
\ usage: <x offset> <y offset> translate
: translate  ( x. y. - )  \ translate every following draw call
  mymatrix -rot 2p>f al_translate_transform
  mymatrix al_use_transform ;

\ ------------------------------------------------------------------------------
\ Fixed point stuff

\ more readable versions of fixed-point conversion ops
subst s>p   1p  subst p>s   1i
subst 2s>p  2p  subst 2p>s  2i
subst 3s>p  3p  subst 3p>s  3i
subst p>s int
subst s>p fixp

\ intent: for 2d vectors.
\ usage: <x.> <y.> 2p.s
: 2p.s  ( x. y. - x. y. )  \ non-destructively print 2 fixed-point numbers.
  cr  2dup 2p. ;

\ intent: for 3d vectors.
\ usage: <x.> <y.> <z.> 3p.s
: 3p.s  ( x. y. z. - x. y. z. )  \ non-destructively print 3 fixed-point numbers.
  cr  3dup 3p. ;

\ intent: for colors and rectangles.
\ usage: <a.> <b.> <c.> <d.> 4p.s
: 4p.s  ( a. b. c. d. - a. b. c. d. )  \ non-destructively print 4 fixed-point numbers.
  cr  2over 2p.  2dup 2p. ;

\ intent: for generating 2d vectors.
\ usage: <x range high> <y range high> 2rnd
: 2rnd  ( n|. n|. - n|. n|. )  \ generate 2 random numbers
  rnd swap rnd swap ;

\ ------------------------------------------------------------------------------
\ Sprite batching

\ intent: should be used only around sprite drawing commands because the effect
\ intent: this mode has on other functions is undefined.
\ intent: also, this is not the most efficient way to batch sprites, but
\ intent: it makes it very easy.
\ usage: BATCH[ ( make some sprite draw commands ) ]BATCH
: batch[  ( - )  \ begin sprite batch ( in Allegro, "hold bitmap drawing")
  true al_hold_bitmap_drawing ;

\ intent: ends batch drawing sequence
\ usage: batch[ ( drawing operations) ]batch
: ]batch  ( - )  \ end sprite batch ( in Allegro, "hold bitmap drawing")
  false al_hold_bitmap_drawing ;

include packed-bits \ bring in the packed bits routines

\ ------------------------------------------------------------------------------
\ Misc.

\ intent: for working with x,y,w,h parameters on the stack.
\ usage: <box box@> 4dup
: 4dup  ( a b c d - a b c d a b c d )  \ duplicate 4 values on the stack.
  2over 2over ;

\ intent: sorts two numbers on the stack
\ usage: <a> <b> lowerupper
: lowerupper  ( n n - n n )  \ put 2 numbers in numerical order.
  2dup > if  swap  then ;

\ intent: get random number between 2 numbers (order unimportant)
\ usage: <a> <b> between
: between  ( n n - n )  \ random number between 2 numbers
  lowerupper  over -  1 +  rnd + ;

[[ ]] constant .. \ {RM} this appears to be an empty pseudoclosure; I assume it's for wasting time?

\ intent: clamp given fixed-point number to the 0.0 ~ 1.0 range.
\ usage: <value.> 1clamp
: 1clamp  ( n. - n. )  \ set n >= 0.0 and n <= 1.0
  0.0 1.0 clamp ;

subst area xyxy  \ this is a more logical name
subst xyxy abab

\ intent: for introducing an element of randomization.  (but it's not the only way to do it; there's no "bias", for example.)
\ usage: <value> <random range> vary
: vary  ( n|. rnd|. - n|. )  \ randomize a number.
  dup 0.5 p*  -rot rnd +  swap - ;

\ intent: for modifying 2d coordinates.
\ usage: <num a> <random range> <num b> <random range> 2vary
: 2vary  ( n|. rnd|. n|. rnd|. - n|. n|. )  \ randomize 2 numbers.
  rot swap vary >r vary r> ;

\ intent: choose between two items randomly
\ usage: <item a> <item b> either
: either  ( a b - a | b )  \ randomly select from 2 items on the stack.
  2 rnd if  drop  else  nip  then ;

\ keyboard arrows
\ intent: used to test if the left arrow key is currently being held
\ usage: left?
: left?  ( - flag )  \ holding left arrow?
  <left> kstate  <pad_4> kstate or ;

\ intent: used to test if the right arrow key is currently being held
\ usage: right?
: right?  ( - flag )  \ holding right arrow?
  <right> kstate  <pad_6> kstate or ;

\ intent: used to test if the up arrow key is currently being held
\ usage: up?
: up?  ( - flag )  \ holding up arrow?
  <up> kstate  <pad_8> kstate or ;

\ intent: used to test if the down arrow key is currently being held
\ usage: down?
: down?  ( - flag )  \ holding down arrow?
  <down> kstate  <pad_2> kstate or ;

\ ----

\ intent: get a point that lies within a box based on proportion
\ usage: <box box@> <xfactor> <yfactor> around
: located  ( x y w h xfactor. yfactor. - x y )  \ find a proportional position within the rectangle (x,y,x+w,y+h)
  2p* 2+ ;

\ intent: get the center of a box
\ usage: <box box@> middle
: middle  ( x y w h - x y )  \ find the center position in the rectangle (x,y,x+w,y+h)
  0.5 0.5 located ;

\ intent: get a random point that lies within a box
\ usage: <box box@> somewhere
: somewhere  ( x y w h - x y )  \ find a random position in the rectangle (x,y,x+w,y+h)
  2rnd 2+ ;

\ intent: box vs box collision
\ usage: <boxa box@> <boxa box@> overlap?
: overlap? ( xyxy xyxy - flag )  \ find if 2 rectangles (x1,y1,x2,y2) and (x3,y3,x4,y4) overlap.
  2swap 2rot rot swap > -rot > or >r rot < -rot swap < or r> or not ;
