\ ------------------------------------------------------------------------------
\ Packed bits tools

\ intent: create a named constant that represents n*2 bits
\ usage: <n> bit <name>
: bit  ( n - <name> n*2 )  \ {RM} I find this a useless word tbh
  dup constant 2 * ;

\ intent: set bits
\ usage: <value> <adr> or!
: or!  ( n adr - )  \ set value at address to ored value
  dup @ rot or swap ! ;

\ intent: reset or mask bits
\ usage: <value> <adr> and!
: and!  ( n adr - )  \ set value at address to anded value
  dup @ rot and swap ! ;

\ intent: toggle bits
\ usage: <value> <adr> xor!
: xor!  ( n adr - )  \ set value at address to xored value
  dup @ rot xor swap ! ;

\ intent: set a specific bit
\ usage: <adr> <bit> set
: set  ( addr n - )  \ turn on a bit
  swap or! ;

\ intent: unset a specific bit
\ usage: <adr> <bit> unset
: unset ( addr n - )  \ turn off a bit
  invert swap and! ;

\ intent: togggle a specific bit
\ usage: <adr> <bit> flip
: flip  ( addr n - )  \ toggle a bit
  swap xor! ;
