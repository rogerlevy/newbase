\ {nodoc} tell the documentation progress monitor tool to ignore this file

0 value tb  \ {brief}

\ intent: {INTENT}
\ usage: {USAGE}
: tb!  ( image - )  \ set current tileset (tilebank)
  to tb ;

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %tileset  \ {brief}
  [[ create-field  does> @ tb + ]] is ifield
  [i
  var img  \ {brief}
  i]
  %box inline tile  \ {brief}

\  var bankcols
\  var bankrows

  \ intent: with DRAW-TILE, instead of giving an index you give an x and y.
  \ intent: this way tilesets can be any size, even mixed sizes in the same image,
  \ intent: and you can do weird things with unaligned offsets.
  \ usage: {USAGE}
  : draw-tile  ( x y flip - )  \ {brief}
    >r  tile dims part!  img @ r> sprite ;

  \ init: this one doesn't set the part's dimensions repeatedly
  \ usage: {USAGE}
  : draw-another-tile  ( x y flip - )  \ {brief}
    >r  partxy!  img @ r> sprite ;

\  : !bankcols  img @ image.w tile width / bankcols ! ;
\  : !bankrows  img @ image.h tile height / bankrows ! ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : tileset.image  ( tileset - image )  \ {brief}
    .img @ ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : tileset.width  ( tileset - w )  \ {brief}
    .img @ image.size drop ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : tileset.height  ( tileset - h )  \ {brief}
    .img @ image.size nip ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : init-tileset  ( image tilew tileh tileset - )  \ {brief}
    tb!  tile dims!  img ! ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : tileset  ( image tilew tileh - <name> )  \ {brief}
    create  %tileset instance init-tileset ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  macro: packtile  ( x y flip - n )  14 << or 16 << or ;  \ {brief}

  \ intent: {INTENT}
  \ usage: {USAGE}
  macro: unpacktile  ( n - x y flip )  dup $ffff and swap 16 >> dup $3fff and swap 14 >> ;  \ {brief}


endp
