\ {nodoc} tell the documentation progress monitor tool to ignore this file
\ ==============================================================================
\ ForestLib > GameBlocks
\ Generic tilemap renderer
\ ========================= copyright 2014 Roger Levy ==========================

\ ==============================================================================
\ maps are 32-bit but the format is user dependent
\ ==============================================================================

absent [][] ?fload 2array_2  \ {brief}

fload tileset  \ {brief}
fload tilemap  \ {brief}

\ intent: {INTENT}
\ usage: {USAGE}
pstruct %plane  \ {brief}
  [i
  %box inline srcbox  \ {brief}
  var tilearray       \ {brief}
  var tm              \ tilemap
  var ts              \ tileset
  %vec2d inline tilestride  \ {brief}
  i]
  %vec2d inline scroll  \ {brief}

  \ intent: {INTENT}
  \ usage: {USAGE}
  : plane.tilemap  ( plane - tilemap )  \ {brief}
    .tm @ ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : plane.tilemap! ( tilemap plane - )  \ {brief}
    .tm ! ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : plane.tileset  ( plane - tileset )  \ {brief}
    .ts @ ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : plane.tileset! ( tileset plane - )  \ {brief}
    with  ts !  ts @ .tile dims  tilestride xy! ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : plane.srcbox   ( plane - box )  \ {brief}
    .srcbox ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : plane.tilestride!  ( x y plane - )  \ {brief}
    .tilestride xy! ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : init-plane ( tilemap tileset plane - )  \ {brief}
    with  s@ plane.tileset!  s@ plane.tilemap!  tm @ tilemap.size srcbox dims! ;

  \ defer rendertile ( pen=xy; tile - )  \ {brief}

  [i
  \ plane cell format:  %?????yyy yyyyyyyy ?????xxx xxxxxxxx  -1 = blank
  \ max tileset image size is 2048x2048
  : tile ( n - )  \ {brief}
    $7ff07ff and  lohi  draw-another-tile ;
  i]

  \ intent: {INTENT}
  \ usage: {USAGE}
  : draw-plane ( pen=xy; plane - )   \ {brief}
    with
    srcbox box@  tilestride xy  tm @ tilemap.array  locals| ta sh sw rows cols |
    cols rows or 0=  if 2drop exit then
    \ ts @ .'rendertile @ is rendertile
    \ ['] draw-tile is rendertile
    ts @ .tile dims partwh!
    batch[
      a@ >r  at@ 2>r
      rows bounds do
        dup i ta [][] a!
        at@
          cols 0 do  @+ tile  sw 0 +at  loop
        sh + at
      loop  drop
      2r> at  r> a!
    ]batch  ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : fitplane  ( w h plane - )  \ calculate rendering dimensions for given width/height in pixels
    with  1 tilestride y u*/ 1 +  srcbox height!  1 tilestride x u*/ 1 +  srcbox width! ;

  \ ask {RL} about the usage of the pen here.
  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilescroll  ( scrollx scrolly planeparams - col row )  \ adjust the drawing pen for scrolling and return the col and row according to given scroll coordinates
    tilestride x 1 max tilestride x!
    tilestride y 1 max tilestride y!
    ( planeparams ) with
    ( scrollx scrolly ) 2dup tilestride y mod negate  swap  tilestride x mod negate  swap  +at
        tilestride y /  swap  tilestride x /  swap ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : draw-plane-scrolling  ( plane - )  \ {brief}
    with  scroll xy  s@ tilescroll  srcbox xy!  s@  draw-plane ;

endp
