\ {nodoc} tell the documentation progress monitor tool to ignore this file
sdltools +order


\ intent: {INTENT}
\ usage: {USAGE}
%asset ~> %tilemap  \ {brief}
  [i
  var ss                                              \ sdlsurface
  %array inline planearr  0 cell planearr init-array  \ declare a tilemap
  i]

  \ intent: {INTENT}
  \ usage: {USAGE}
  : !tilemaparray  ( - )  \ {brief}
    ss @ dup >pixels planearr items!
      sdl-surface-dims
      2dup planearr .cols 2!  *
      planearr with
      dup maxitems !  #items ! ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilemap.size ( tilemap - #cols #rows )  \ {brief}
    .ss @ sdl-surface-dims ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilemap.w  ( tilemap - #cols )  \ {brief}
    .ss @ SDL_Surface w @ ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilemap.h  ( tilemap - #rows )  \ {brief}
    .ss @ SDL_Surface h @ ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilemap.surface  ( tilemap - SDL_Surface )  \ {brief}
    .ss @ ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilemap.surface!  ( SDL_SURFACE tilemap - )  \ {brief}
    with ss ! !tilemaparray ;
  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilemap.array  ( tilemap - 2array )  \ {brief}
    .planearr ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : new-tilemap  ( w h tilemap - )  \ {brief}
    with  32 create-sdl-surface ss !  !tilemaparray ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : free-tilemap  ( tilemap - )  \ {brief}
    .ss  dup @ SDL_FreeSurface  off ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : load-tilemap  ( addr c tilemap - )  \ {brief}
    dup free-tilemap with  2dup load-sdl-surface ss !  path place  !tilemaparray ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilemap:onload  ( addr c - )  \ image file
    2dup s@ ['] load-tilemap catch
      if  cr  type  true abort" Error in TILEMAP:ONLOAD : There was a problem loading an image."  then
    2drop ;

  ' tilemap:onload onload !

  \ intent: {INTENT}
  \ usage: {USAGE}
  : resize-tilemap  ( w h tilemap - )  \ {brief}
    with  ss @ -rot resize-sdl-surface ss !  !tilemaparray ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : tilemap  ( - <name> <filespec> )  \ declare a tilemap
    create  namespec  %tilemap instance with  asset^ ;

  \ intent: {INTENT}
  \ usage: {USAGE}
  : copy-tilemap  ( src dest - )  \ {brief}
    >r  .ss @ clone-sdl-surface  r> tilemap.surface! ;

endp
