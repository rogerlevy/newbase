\ ==============================================================================
\ ForestLib > GameBlocks [2] > Allegro5 I/O [2]
\ Keyboard, mouse, and joystick polling and state [2]
\ ========================= copyright 2014 Roger Levy ==========================

\ RM: I have moved the various device implementations into their own files
\ for clarity of reading those functions and to make x-ref easier.

\ this file's purpose is for any input code that should be
\ introduced AFTER the device specific code.

\ intent: initialize input module state data
\ usage: init-input
: init-input  ( - )  \ initalize input module
  (init-joystate) ;
