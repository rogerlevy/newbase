\ ==============================================================================
\ ForestLib > GameBlocks [2]
\ Box data structure
\ ========================= copyright 2014 Roger Levy ==========================

\ intent: fetch four sequential values from address
\ usage: <obj> 4@
: 4@  ( adr - a b c d )  \ read 4 values
  a!>  @+ @+ @+ @+ ;

\ intent: store four values starting at address
\ usage: <a> <b> <c> <d> <obj> 4!
: 4!  ( x y w h adr - )  \ write 4 values
  a!>  2swap swap !+ !+ swap !+ !+ ;

\ intent: fetch and print four sequential values from address
\ usage: <obj> 4?
: 4?  ( adr - )  \ print 4 values
  a!>  @+ . @+ . @+ . @+ . ;

\ intent: define a box object component
\ usage: %box object <name>
%vec2d ~> %box  \ box inherits from vec2d giving the x/y component of the box
  [i
    var width   \ width of the box
    var height  \ height of the box
  i]

  subst .width .width    \ public external version of field
  subst .height .height  \ public external version of field

  \ getters n' setters
  macro: width  .width @ ;  ( box - w|. )
  macro: width!  .width ! ;  ( w|. box - )
  macro: +width  .width +! ;  ( w|. box - )

  macro: height  .height @ ;  ( box - h|. )
  macro: height! .height ! ;  ( h|. box - )
  macro: +height .height +! ;  ( h|. box - )

  macro: dims  .width 2@ ;  ( box - w|. h|. )
  macro: dims!  .width 2! ;  ( w|. h|. box - )
  macro: +dims  .width 2+! ;  ( w|. h|. box - )

  macro: box@  4@ ;  ( box - x|. y|. w|. h|. )
  macro: box!  4! ;  ( x|. y|. w|. h|. box - )
  macro: box?  4? ;  ( box - )

  \ intent: copy properties of one box to another
  \ usage: <source box> <destination box> box.copy
  : box.copy  ( src-box dest-box - )  \ clone a box
    4 imove ;

  \ intent: add the properties of two boxes
  \ usage: <source box> <destination box> box.add
  : box.add   ( src-box dest-box - )  \ adds two boxes
    >r  4@  r@ +dims  r> +xy ;

  \ intent: clears the properties of a box to zero
  \ usage: <box> box.clear
  : box.clear  ( box - )  \ reset box properties
    4 0 ifill ;

endp
