\ ==============================================================================
\ ForestLib > GameBlocks [2] > Allegro5 I/O [2]
\ Image struct
\ ========================= copyright 2014 Roger Levy ==========================

variable smooth-textures \ used by ?smooth for setting appropriate texture smoothing flags

\ intent: sets proper texture smoothing flags based on the smooth-textures variable when creating new bitmaps
\ usage: ?smooth
: ?smooth  ( - )  \ enable/disable smoothing for new bitmap
  al_get_new_bitmap_flags
  smooth-textures @ if
    ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR or
  else
    [ ALLEGRO_MIN_LINEAR ALLEGRO_MAG_LINEAR or invert ]# and
  then
  al_set_new_bitmap_flags ;

\ intent: get dimensions of bitmap
\ usage: <bitmap address> al-bitmap-size
: al-bitmap-size  ( ALLEGRO_BITMAP - w h )  \ get size of allegro bitmap
  dup al_get_bitmap_width  swap al_get_bitmap_height ;

%asset ~> %image  \ image asset class
  [i
  var albmp  \ allegro bitmap container
  i]

  \ intent: load the specified image into the given image structure
  \ usage: <filename> <image> load-image
  : load-image ( addr c image - )  \ load an image
    ?smooth  with  2dup path place  z$  al_load_bitmap  albmp ! ;

  \ intent: save the given image back to the original filename (stored in image asset structure)
  \ usage: <image> save-image
  : save-image ( image - )  \ overwrite image file
    with  path count  cr  2dup type  z$  albmp @ al_save_bitmap  0= abort" There was a problem saving an image" ;

  \ intent: save the given image to given filename
  \ usage: <filename> <image> save-image-as
  : save-image-as ( addr c image - )  \ save image to specific file
    with  path place  s@ save-image ;

  \ intent: asset onload handler
  \ usage: {not used externally}
  : image:onload  ( adr c - )  \ load image and abort on error
    2dup s@ load-image
    albmp @ 0= if  cr  type  true abort" Error in BITMAP:ONLOAD : There was a problem loading an image."  then
    2drop ;
  \ set asset onload to the image onload handler
  ' image:onload onload !

  \ intent: get dimensions of given image
  \ usage: <image> image.size
  : image.size  ( image - w h )  \ get size of image
    .albmp @ al-bitmap-size ;
    
  \ intent: get width of given image
  \ usage: <image> image.w
  : image.w  ( image - w )  \ get width of image
    image.size drop ;
    
  \ intent: get height of given image
  \ usage: <image> image.h
  : image.h  ( image - h )  \ get height of image
    image.size nip ;

  \ intent: fetch the internal allegro bitmap from the given image
  \ usage: <image> allegro-bitmap
  : allegro-bitmap  ( image - ALLEGRO_BITMAP )  \ get allegro bitmap from image
    .albmp @ ;

  \ no longer used
  \ : entire ( image - image )  \ 
  \   0e 0e  dup  image.size 2s>f  4sfparms  allegro-region 4! ;

  \ intent: create an image instance
  \ usage: image <instance name> <filespec>
  : image ( - <name> <filespec> )  \ create image
    create  namespec  %image instance with  asset^ ;

\ ------------------------------------------------------------------------------

  \ intent: store two float values as single-precision floats in given address
  \ usage: <x> <y> 2s>f <addr> 2sf!
  : 2sf!  ( f: x y - ) ( s: adr - )  \ store two floats in memory
    dup >r cell+ sf! r> sf! ;

  \ intent: store two fixed point values as single-precision floats in given address
  \ usage: <x.> <y.> <addr> p2sf!
  : p2sf!  ( x. y. adr - )  \ store two fixed points in memory
    >r  p>f  r@ cell+ sf!  p>f  r> sf! ;

  \ call these before SPRITE etc
  
  \ intent: sets the source origin of the rectangle 'art' of image which will be drawn by sprite
  \ usage: <x> <y> partxy!
  : partxy!  ( x y - )  \ set render 'part' source origin
    2s>f  allegro-region 2sf! ;
    
  \ intent: sets the source size of the rectangle 'part' of image which will be drawn by sprite
  \ usage: <width> <height> partwh!
  : partwh!  ( w h - )  \ set render 'part' source size
    2s>f allegro-region cell+ cell+ 2sf! ;
    
  \ intent: sets the rectangle 'part' of image which will be drawn by sprite
  \ usage: <x> <y> <width> <height> part!
  : part!  ( x y w h - )  \ set render 'part'
    partwh! partxy! ;

  \ intent: easy API, okay performance
  \ usage: <image> <flip> blit
  : blit ( image flip - )  \ draw entire image
    >r  allegro-bitmap  r> al-draw-bitmap ;

  \ intent: easy API, okay performance
  \ usage: call PART! etc first
  : sprite ( image flip - )  \ draw a portion of an image
    >r  allegro-bitmap r> al-draw-bitmap-region ;

  \ fancy transformed blit routines
  \ easy API, okay performance
  \ make certain that you call transformed before using the following
  \ - tsprite
  \ - tblit

  \ intent: draw a portion of an image with scaling and rotation transformations and color tinting
  \ usage: call PART! (or friends) and TRANSFORMED first
  : tsprite ( image flip - )  \ scaled, rotated, and tinted sprite (part of bitmap)
    >r  allegro-bitmap r> al-draw-tinted-scaled-rotated-bitmap-region ;

  \ intent: draw entire image with scaling and rotation transformations and color tinting
  \ usage: <image> <flip> tblit
  : tblit ( image flip - )  \ scaled, rotated, and tinted blit
    0e 0e  dup  image.size 2s>f  4sfparms  allegro-region 4!
    tsprite ;

endp
