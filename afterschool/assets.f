\ ==============================================================================
\ ForestLib > GameBlocks
\ Asset objects
\ ========================= copyright 2014 Roger Levy ==========================

create assets  500 stack,   \ project asset list
true ?constant log-assets   \ flag that controls if asset loading is logged; default is true

\ intent: define the base asset class for all asset components
\ usage: %asset ~> %<subclass name>
pstruct %asset  \ base class for all assets
  noop: onload                                   \ method called when asset is loaded
  noop: ondestroy                                \ method called when asset is destroyed
  256 field path                                 \ file path to asset
  [[ path count ]] var: 'fetchpath  ( - adr c )  \ used in fetching asset path
  var assetid                                    \ unique numerical ID of asset (also index into assets list)
endp

\ intent: get path to file {RM: this is either BROKEN or not tested in the correct context}
\ usage: <path string> find-asset
: find-asset  ( path c - path c )  \ if the file is not found where specified from the current directory, check the home directory
  ?homepath ;

\ intent: get the static name string of untitled assets
\ usage: untitled
: untitled  ( - adr c )  \ untitled asset name
  " newasset" ;

\ intent: tests to see if asset is untitled
\ usage: <asset name> untitled?
: untitled?  ( adr c - adr c flag )  \ check if asset is untitled
  2dup " newasset" -match  nip  0= ;

\ intent: fetch the path to the asset
\ usage: path@
: path@  ( - adr c )  \ fetch path of asset
  'fetchpath @ execute ;

\ intent: print a report of the asset being loaded
\ usage: <path to asset> ?log
: ?log  ( - )  \ print asset load report
  log-assets if
    untitled? if
      report" Untitled asset " s@ .
    else
      report" Asset: "  2dup -path type  space
    then
  then ;

\ intent: display a nicely formated time value
\ usage: <time> time.
: time.  ( time - )  \ pretty-print a time value
  50 get-xy nip at-xy
  ( time ) 1000 /  dup 4 .r  ."  ms "
  ( time ) 5 + 10 /  0 ?do  [char] * emit  loop ;

\ intent: attempt to load an asset - calls onload
\ usage: {not to be used externally}
: (load)  ( - )  \ attempt to load an asset
  path@ find-asset ?log  [[ onload @ execute ]] exectime time. ;

\ intent: add asset to asset list and load the asset
\ usage: <path to asset> asset^
: asset^  ( path c - )  \ add asset to assets list
  path place  assets length assetid !  s@ assets push  (load) ;

\ intent: load all known assets into assets list
\ usage: load-assets
: load-assets  ( - )  \ load known assets
  [[ with (load) ]] assets those ;

\ intent: load a given asset
\ usage: <asset> load-asset
: load-asset  ( asset - )  \ load asset
  with  (load) ;

\ intent: destroy (unload) given asset
\ usage: <asset> destroy-asset
: destroy-asset  ( asset - )  \ destroy asset
  with  ondestroy @ execute ;

\ intent: clears the assets list of all loaded assets
\ usage: clear-assets
: clear-assets  ( - )  \ empty the assets list
  [[ destroy-asset ]] assets those  assets vacate ;

\ intent: reload given asset
\ usage: {not to be used externally} <asset> (reload)
: (reload)  ( asset - )  \ reload asset
  with  ondestroy @ execute  (load) ;

\ intent: destroy and load all known assets
\ usage: reload-assets
: reload-assets  ( - )  \ reload assets
  [[ (reload) ]] assets those ;

\ intent: print a list of loaded assets
\ usage: .assets
: .assets  ( - )  \ list loaded assets
  [[ cr  with  path@ type ]] assets those ;

\ intent: creating a new asset identifier {RM: I cannot figure out how to get this to work right}
\ usage: identifier <name>
: identifier  ( - <name> )  \ create new identifier
  bl parse  bl skip  2dup -path  $create ;

\ intent: check if an asset has been loaded
\ usage: <path> loaded
: loaded  ( path c - 0 | asset )  \ asset loaded test
  locals| c p |
  0 [[ with  path count  p c compare  0= if  drop  s@  then ]] assets those ;

\ intent: check if an asset has been loaded (for use with if statements, as this leaves a flag on tos)
\ usage: <path> ?loaded
: ?loaded ( path c - asset true | path c false )  \ asset loaded test
  locals| c path |
  path c loaded  ?dup if  true  exit  else  path  c  false  then ;

\ Synchronous progress bar support
\ ask {RL} for an explanation

defer (update-loader)  ( - )  \ called by load-assets-showing-progress in order to provide visual load feedback

\ intent: assign a callback for showing asset load progress bar
\ usage: <xt> load-assets-showing-progress
: load-assets-showing-progress  ( xt - )  \ call xt for each asset loaded
  is (update-loader)  [[ with  (update-loader)  (load) ]] assets those ;

\ intent: gives percentage of asset load progress in 0.0-1.0 range
\ usage: progress%
: progress%  ( - n. )  \ tool for making loading screens; returns a value 0.0 ~ 1.0 for the current asset (in STRUCT)
  1.0  assetid @  assets length  1 -  */ ;
