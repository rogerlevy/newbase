
1 constant lb  \ left mouse button id
2 constant rb  \ right mouse button id
3 constant mb  \ middle mouse button id

create mouse-state /ALLEGRO_MOUSE_STATE /allot   \ current frame's state
create mickey-state /ALLEGRO_MOUSE_STATE /allot  \ last frame's state

\ intent: get current mouse coordinates
\ usage: mouse
: mouse   ( - x y )  \ get mouse coordinates
  mouse-state 2@ ;
  
\ intent: get how far the mouse has moved since the last time it was polled
\ usage: mickey
: mickey  ( - x y )  \ get mouse movement
  mickey-state 2@ ;
  
\ intent: obtain the mouse movement delta
\ usage: mdelta
: mdelta  ( - x y )  \ get inverse mouse movement
  mouse mickey 2- ;

\ intent: poll the mouse device for changes in state and updates global mouse state variables
\ usage: poll-mouse
: poll-mouse  ( - )  \ poll mouse device
  mouse-state  dup mickey-state /ALLEGRO_MOUSE_STATE move  al_get_mouse_state ;

\ intent: check the state of given mouse button
\ usage: <button id> mstate
: mstate  ( button# - flag )  \ is mouse button down?
  mouse-state swap al_mouse_button_down 0<> ;

\ intent: check if given mouse button was down in the previous frame
\ usage: <button id> mlast
: mlast  ( button# - flag )  \ was mouse button down?
  mickey-state swap al_mouse_button_down 0<> ;

\ intent: get the delta of the mouse button
\ usage: <button id> mbdelta
: mbdelta  ( button# - delta )  \ delta is -1, 0, or 1
  >r  r@ mlast  r> mstate  - ;

\ intent: check if given mouse button was pressed in this frame
\ usage: <button id> mpressed
: mpressed  ( button# - flag )  \ was mouse button pressed?
  mbdelta 1 = ;

\ intent: check if the mouse button was released in this frame
\ usage: <button id> mreleased
: mreleased  ( button# - flag )  \ was mouse button released?
  mbdelta -1 = ;
