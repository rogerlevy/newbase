
create kbstate  /ALLEGRO_KEYBOARD_STATE /allot \ current frame's state
create kblast  /ALLEGRO_KEYBOARD_STATE /allot  \ last frame's state

\ intent: poll the keyboard device for changes in state and updates global keyboard state variables
\ usage: poll-keyboard
: poll-keyboard  ( - )  \ poll keyboard device
  kbstate kblast /ALLEGRO_KEYBOARD_STATE move
  kbstate al_get_keyboard_state ;

\ intent: check if given bit is set in given value
\ usage: <bit position> <value> bit?
: bit?  ( bit val - flag )  \ is bit set?
  swap 1 swap << and 0<> ;

\ intent: check state of <bit> in <addr>
\ usage: <bit> <addr> set?
: set?  ( bit addr - flag )  \ get state of bit
  cell+ >r  32 /mod cells  r> +  @  bit? ;

\ intent: get the state of the given key in the previous frame
\ usage: <keycode> klast
: klast  ( key# - flag )  \ key state in previous frame
  kblast set? ;

\ intent: get the state of the given key at this frame
\ usage: <keycode> kstate
: kstate  ( key# - flag )  \ get key state
  kbstate set? ;

\ intent: get delta of the given key
\ usage: <keycode> kdelta
: kdelta  ( key# - delta )  \ delta is -1, 0, or 1
  >r  r@ klast  r> kstate  - ;

\ intent: check if given key was pressed in this frame
\ usage: <keycode> kpressed
: kpressed  ( key# - flag )  \ was key pressed?
  kdelta 1 = ;

\ intent: check if given key was released in this frame
\ usage: <keycode> kreleased
: kreleased  ( key# - flag )  \ was key released?
  kdelta -1 = ;

\ MODIFIER KEYS

\ intent: checks if either alt key is being held
\ usage: alt?
: alt?  ( - flag )  \ is alt down?
  <alt> kstate  <altgr> kstate  or ;

\ intent: checks is either ctrl key is being held
\ usage: ctrl?
: ctrl?  ( - flag )  \ is ctrl down?
  <lctrl> kstate  <rctrl> kstate  or ;

\ intent: checks if either shift key is being held
\ usage: shift?
: shift?  ( - flag )  \ is shift down?
  <lshift> kstate  <rshift> kstate  or ;
