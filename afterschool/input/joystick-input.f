
16 constant MAX_JOYSTICKS                 \ maximum number of joysticks
create joyhandles  MAX_JOYSTICKS stack,   \ stack of handles to joysticks

\  currently doesn't handle connecting/disconnecting devices in-game, but Allegro 5 /does/ support this (via an event)
create joysticks  MAX_JOYSTICKS /ALLEGRO_JOYSTICK_STATE array,  \ array of joystick state objects

\ intent: fills the joyhandles stack with handles to all connected joystick devices
\ usage: {not used externally}
: (init-joystate)  ( - )  \ init joystick 'state' data
  al_get_num_joysticks 0 ?do  i al_get_joystick joyhandles push  loop ;

\ intent: get the address of given joystick's stick state data
\ usage: <joy id> <stick id> joy
: joy ( joy# stick# - vector )  \ get address of given joystick stick
  /ALLEGRO_JOYSTICK_STATE_STICK *  swap joysticks [] .ALLEGRO_JOYSTICK_STATE-sticks + ;

\ intent: get button value of given joystick
\ usage: <joy id> <button id> joybtn
: joybtn ( joy# button# - 0-32767 )  \ get button value of given joystick
  cells swap joysticks [] .ALLEGRO_JOYSTICK_STATE-buttons  +  @ ;

\ intent: ???? {RM: Roger, I need you to go over this mess with me.}
\ usage: @f>p+
: @f>p+  ( - )  \ ????
  a@ sf@ f>p !+ ;

\ intent: poll the joystick devices for changes in state and update global joystick state variables
\ usage: poll-joysticks
: poll-joysticks  ( - )  \ poll joystick devices
  a@
  al_get_num_joysticks 0 ?do
    i joyhandles [] @  i joysticks []  al_get_joystick_state
    _AL_MAX_JOYSTICK_STICKS 0 do  j i joy a!  @f>p+ @f>p+ @f>p+  loop
  loop
  a! ;
