
%vec2d object offset  \ drawing coordinate offset vector (uses integer vector)

\ intent: add <v1> (integer 2D vector) to offset vector
\ usage: <v1> offset+
: offset+  ( x y - x y )  \ add to drawing offset
  offset xy 2+ ;
  
\ intent: subtract <v1> (integer 2D vector) from offset vector
\ usage: <v1> offset-
: offset-  ( x y - x y )  \ subtract from drawing offset
  offset xy 2- ;

\ intent: subtract <v1> (fixed-point 2D vector) from offset vector
\ usage: <v1> offset-(p)
: offset-(p)  ( x. y. - x. y. )  \ subtract from drawing offset (fixed-point)
  offset xy 2s>p 2- ;

\ intent: set allegro pen position vector to <V1> (integer 2D vector)
\ usage: <v1> at
: at  ( x y - )  \ set pen position
  offset+ 2?p p>f allegro-pen .y sf!  p>f allegro-pen .x sf! ;

\ intent: add <v1> (integer 2D vector) to allegro pen position
\ usage: <v1> +at
: +at  ( x y - )  \ add to pen position
  2?p p>f allegro-pen .y sf+!  p>f allegro-pen .x sf+! ;

\ intent: get the position vector of the allegro pen
\ usage: at@
: at@  ( - x y )  \ get pen position
  allegro-pen .x sf@ f>s allegro-pen .y sf@ f>s offset- ;

\ intent: get the position vector of the allegro pen in fixed point coordinates
\ usage: at@p
: at@p  ( - x. y. )  \ get pen position (fixed-point)
  allegro-pen .x sf@ f>p allegro-pen .y sf@ f>p offset-(p) ;

\ intent: get position vector of the allegro pen plus <v1> (integer 2D vector)
\ usage: <v1> at+
: at+  ( x y - x y )  \ get pen position adjacent to given vector
  allegro-pen .x sf@ f>s allegro-pen .y sf@ f>s 2+ ;

\ intent: set allegro pen thickness
\ usage: <thickness.> thick
: thick  ( n. - )  \ set pen thickness
  p>f allegro-thickness f! ;

\ intent: set allegro pen transformation with given transform object
\ usage: <transform> transformed
: transformed  ( transform - )  \ pen transformation
  with
  scale xy 2p>f 2sfparms allegro-scale xy!
  s@ angle p>f allegro-angle sf!
  tint rgba 4p>f 4sfparms allegro-color 4! ;

\ intent: set allegro pen transformation to 'identity'
\ usage: untransformed
: untransformed  ( - )  \ identity pen transformation
  1e 1e 2sfparms allegro-scale xy!
  0e allegro-angle sf!
  1e 1e 1e 1e 4sfparms allegro-color 4! ;

\ intent: set the allegro pen color to given rgba quad (fixed point values)
\ usage: <rgba.> pcolor
: pcolor  ( r. g. b. a. - )  \ set pen color
  allegro-color a!>  >r >r >r  p>f sf!+  r> p>f sf!+  r> p>f sf!+  r> p>f sf!+ ;

\ intent: get the current allegro pen color
\ usage: pcolor@
: pcolor@  ( - r. g. b. a. )  \ get pen color
  allegro-color 4@ 4f>p ;
