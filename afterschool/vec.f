\ ==============================================================================
\ ForestLib > GameBlocks [2]
\ Vector structs
\ ========================= copyright 2014 Roger Levy ==========================

pstruct %vec2d  \ 2D vector class
  var x         \ X axis component
  var y         \ Y axis component

  \ getters n' setters
  macro: x   ( vec2d - n|. )   .x @ ;  \ get X component
  macro: x!  ( n|. vec2d - )   .x ! ;  \ set X component
  macro: +x  ( n|. vec2d - )   .x +! ; \ add scalar to X component
  macro: y   ( vec2d - n|. )   .y @ ;  \ get Y component
  macro: y!  ( n|. vec2d - )   .y ! ;  \ set Y component
  macro: +y  ( n|. vec2d - )   .y +! ; \ add scalar to Y component
  macro: xy  ( vec2d - x|. y|. ) 2@ ;  \ get X and Y components
  macro: xy! ( x|. y|. vec2d - ) 2! ;  \ set X and Y components
  macro: +xy ( x|. y|. vec2d - ) 2+! ; \ add scalars to vector

  \ intent: print the component values of the vector to standard output
  \ usage: <vec> xy?
  : xy?  ( vec2d - )  \ print vector
    2p? ;

  \ intent: scale vector by another vector's component values
  \ usage: <v1> <vec> vec2d.scale
  : vec2d.scale  ( x. y. vec2d - )  \ scale vector
    dup >r 2@ scale2d r> 2! ;

  \ intent: set vector components to zero
  \ usage: <vec> vec2d.clear
  macro: vec2d.clear  ( vec2d - ) 2 0 ifill ;  \ zero-out vector

  \ intent: copy one vector into another
  \ usage: <source> <dest> vec2d.copy
  macro: vec2d.copy  ( src-vec2d dest-vec2d - ) 2 imove ;  \ clone vector

endp

%vec2d ~> %vec3d  \ 3D vector class
  var z

  \ getters n' setters
  macro: z   ( vec3d - n|. )  .z @ ;          \ get Z component
  macro: z!  ( n|. vec3d - )  .z ! ;          \ set Z component
  macro: +z  ( n|. vec3d - )  .z +! ;         \ add scalar to Z component

  macro: xyz   ( vec3d - x|. y|. z|. )  3@ ;  \ get vector triad
  macro: xyz!  ( x|. y|. z|. vec3d - )  3! ;  \ set vector triad
  macro: +xyz  ( x|. y|. z|. vec3d - )  3+! ; \ add scalars to vector

  \ intent: print the component values of the vector to standard output
  \ usage: <vec> xyz?
  : xyz?  ( vec3d - )  \ print vector
    3p? ;

\  : vec3d.scale   with x 3@ scale3d x 3! ;

  \ intent: set vector components to zero
  \ usage: <vec> vec3d.clear
  macro: vec3d.clear  ( vec3d - )  3 0 ifill ;  \ zero-out vector

  \ intent: copy one vector into another
  \ usage: <source> <dest> vec3d.copy
  macro: vec3d.copy  ( src-vec3d dest-vec3d - )  3 imove ;  \ clone vector

endp
