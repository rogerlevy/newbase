\ {nodoc}

fload tally \ bring in tallying utility
fload files \ bring in file utilities

\ intent: count instances of a string in a given file
\ usage: <string to find> <filename to search> #instances
: #instances  ( needle c filename c - n )  \ tally instances
  file@ over >r tally r> free throw ;

\ intent: print out length repetitions of given string
\ usage: <string> <length> prints
: prints  ( adr c n - )
  0 ?do 2dup type loop 2drop ;

variable #complete \ completion total
variable #total \ total needed to be completed

: (?file-should-be-ignored)  ( filename c - flag )  \ check file for a "{nodoc}" tag
  2>r  " {nodoc}" 2r@ #instances  0= not  2r> 2drop ;

\ intent: find the documentation completion progress for a given file
\ usage: <path> fileprogress
: fileprogress  ( filename c - )  \ calculate progress for a file
  2>r
  cr 2r@ -path type space  get-xy nip 40 swap at-xy
  ." ["  get-xy
  " intent:" 2r@ #instances   " usage:" 2r@ #instances  +
  " {intent}" 2r@ #instances   " {usage}" 2r@ #instances  +
  \ " {-intent}" 2r@ #instances   " {-usage}" 2r@ #instances  +
  " {brief}"  2r@ #instances
  2r> 2drop
  locals| #{briefs} #{preludes} #preludes |

  #preludes  #{preludes} +  #{briefs} +  0= if
    bright ." NO STUBS OR PRELUDES EXIST" normal
  else
    #preludes  #{preludes} #{briefs} +
    over +
      2dup  #total +!  #complete +!
      ?dup if
        2dup >= if
          2drop bright ." ======= FINISHED \^o^/ =======" normal
        else
          30 swap */  " =" rot 30 min prints
        then
      else
        drop
      then
  then
  30 0 2+ at-xy  ." ]" ;

\ intent: process the file tree to find the documentation completion percentage
\ usage: progress
: progress  ( - )  \ show documentation progress
  0 #complete !  0 #total !
  cwd [[
    is-dir not if
      .filename zcount 2dup -path " .f" search nip nip if
        2dup (?file-should-be-ignored) if
          bold cr ." Skipping " -path type normal
        else
          fileprogress
        then
      else
        2drop
      then
    else drop then
    false
  ]] 0
  [[ zcount -path drop c@ [char] . <> ]] directorieswalker2
  cr  ." Total progress: "  #complete @ #total @ 100 swap */ . ." %"
  ;


variable incomplete

: filetallies  ( filename c - )  \ just show remaining doc work to be done
  2>r
  cr 2r@ -path type space get-xy nip 30 swap at-xy
  " {intent}" 2r@ #instances
  " {usage}" 2r@ #instances +
  " {brief}" 2r> #instances +
  ?dup if
    dup .  incomplete +!  ." remaining"
  else
    bright ." ======= FINISHED \^o^/ =======" normal
  then
;

: tallies  ( - )
  0 incomplete !
  cwd [[
    is-dir not if
      .filename zcount 2dup -path " .f" search nip nip if
        2dup (?file-should-be-ignored) if
          bold cr ." Skipping " -path type normal
        else
          filetallies
        then
      else
        2drop
      then
    else drop then
    false
  ]] 0
  [[ zcount -path drop c@ [char] . <> ]] directorieswalker2
  cr  ." Total remaining: "  incomplete ? ;
