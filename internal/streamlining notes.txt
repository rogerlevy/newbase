The ability to write anything and have it guaranteed to work?

- graphics
- sound
- game logic




--- Layers ---


The Hardware
======================================
Drivers
======================================
OS  |  OpenGL  |  DirectX  | OpenAL
======================================
Allegro 5
======================================
afterschool



Common stuff:
    OpenGL
    Allegro 5


Classes:
    image -> allegro bitmap -> gl texture
    sample -> allegro sample -> openal
    inputmap -> allegro functions -> directx
    actor
        transform (base)
        extant
        vtable
            onstep
            onpost
            ondraw
        evtable
            enabled?
            visible?
            intersects?

        children
        parent

        myname
        myid
        mytype

        tempdepth
        tempdata

        deleted
        timer
        lifetime

        specific stuff?
            bmode
            stays
            invuln
            hp
            cflags
            hitbox
            'act
            'post

