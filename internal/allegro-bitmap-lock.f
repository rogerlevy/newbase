\ {nodoc} tell the documentation progress monitor tool to ignore this file

\ WIP; absolutely untested

\ intent: {INTENT}
\ usage: {USAGE}
: al-bitmap-lock  ( ALLEGRO_BITMAP - ALLEGRO_BITMAP pitch addr )  \ {brief}
  dup allegro-lock-format ( ALLEGRO_PIXEL_FORMAT_ANY ) allegro-lock-mode al_lock_bitmap
  dup .ALLEGRO_LOCKED_REGION-pitch @  swap .ALLEGRO_LOCKED_REGION-data @ ;
