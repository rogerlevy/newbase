\ {nodoc} tell the documentation progress monitor tool to ignore this file

\ --- change cwd to that of this file, saving the current one
256 allocate throw constant ztemp
256 allocate throw constant ztemp2

\ SwiftForth:
[defined] config-os-windows [if]
  255 ztemp2 GetCurrentDirectory drop
  INCLUDING -NAME ztemp zplace
  ztemp SetCurrentDirectory drop
[then]

\ Laod files
cd atoc
include atoc.f
cd ../
old-structs
fload allegro5-1-nofloatparams
new-structs
fload opengl-lite
fload sdl
fload afterschool

\ restore previous (calling) folder
ztemp2 SetCurrentDirectory drop

\ get rid of temporary path buffers
ztemp free throw smudge ztemp
ztemp2 free throw smudge ztemp2
