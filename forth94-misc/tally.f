
\ intent: count instances of a string in another string
\ usage: <string to search> <string to find> tally
: tally  ( haystack-adr count needle-adr c - #instances )
  0 locals| n |
  begin
    2>r 2dup 2r> 2swap search(nc) while
    1 +to n  1 u+ 1 -
  repeat
  2drop 2drop n ;
