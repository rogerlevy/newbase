
VARIABLE _powers \ holds number of powers of two 

\ intent: obtain the next power-of-two in which the given number will fit
\ usage: <number> power/2
: power/2  ( n - n )  \ next power of 2
  0 _powers !   1 begin over over > while 1 _powers +! 2* repeat nip ;

\ intent: tests if the given number is a power of two
\ usage: <number> power/2?
: power/2?  ( n - flag )  \ check for power of two
  dup power/2 = ;

\ intent: obtain the number of powers of two a number is
\ usage: <number> powers/2
: powers/2  ( n - n )  \ get number of powers of two 
  power/2 drop _powers @ ;
